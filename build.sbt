import _root_.sbt.Keys._

name := "escape"

version := "0.1"

scalaVersion := "2.13.1"

scalacOptions := List(
  "-encoding",
  "utf8",
  "-feature",
  "-unchecked",
  "-deprecation",
  "-target:jvm-1.8",
  "-language:_",
  "-Ymacro-annotations"
)

val circeVersion      = "0.12.1"
val circeDerVersion   = "0.12.0-M7"
val catsVersion       = "2.0.0"
val catsEffectVersion = "2.0.0"
val zioVersion        = "1.0.0-RC18-2"
val zioCatsVersion    = "2.0.0.0-RC12"
val tofuVersion       = "0.7.4"
val tschemaVersion    = "0.12.2"
val derevoVersion     = "0.11.2"
val pureconfigVersion = "0.12.3"
val doobieVersion     = "0.8.8"
val logbackVersion    = "1.2.3"
val monocleVersion    = "2.0.0"

libraryDependencies += "ch.qos.logback"  % "logback-classic"             % logbackVersion
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging"   % "3.9.2"
libraryDependencies += "dev.zio"        %% "zio"                         % zioVersion
libraryDependencies += "dev.zio"        %% "zio-streams"                 % zioVersion
libraryDependencies += "dev.zio"        %% "zio-interop-cats"            % zioCatsVersion
libraryDependencies += "io.circe"       %% "circe-core"                  % circeVersion
libraryDependencies += "io.circe"       %% "circe-generic"               % circeVersion
libraryDependencies += "io.circe"       %% "circe-parser"                % circeVersion
libraryDependencies += "io.circe"       %% "circe-generic-extras"        % circeVersion
libraryDependencies += "io.circe"       %% "circe-derivation"            % circeDerVersion
libraryDependencies += "io.circe"       %% "circe-derivation-annotations" % circeDerVersion
libraryDependencies += "org.manatki"    %% "derevo-cats"                 % derevoVersion
libraryDependencies += "org.manatki"    %% "derevo-circe"                % derevoVersion
libraryDependencies += "org.manatki"    %% "derevo-pureconfig"           % derevoVersion
libraryDependencies += "org.mockito"     % "mockito-core"                % "3.0.0" % "test"
libraryDependencies += "org.scalacheck" %% "scalacheck"                  % "1.14.1-RC2" % "test"
libraryDependencies += "org.scalactic"  %% "scalactic"                   % "3.0.8"
libraryDependencies += "org.scalatest"  %% "scalatest"                   % "3.0.8" % "test"
libraryDependencies += "org.tpolecat"   %% "doobie-core"                 % doobieVersion
libraryDependencies += "org.tpolecat"   %% "doobie-h2"                   % doobieVersion
libraryDependencies += "org.typelevel"  %% "discipline-scalatest"        % "1.0.1"
libraryDependencies += "org.typelevel"  %% "cats-core"                   % catsVersion
libraryDependencies += "org.typelevel"  %% "cats-free"                   % catsVersion
libraryDependencies += "org.typelevel"  %% "cats-laws"                   % catsVersion
libraryDependencies += "org.typelevel"  %% "cats-laws"                   % catsVersion % "test"
libraryDependencies += "org.typelevel"  %% "cats-effect"                 % catsEffectVersion
libraryDependencies += "ru.tinkoff"     %% "tofu"                        % tofuVersion
libraryDependencies += "ru.tinkoff"     %% "tofu-config"                 % tofuVersion
libraryDependencies += "ru.tinkoff"     %% "tofu-logging"                % tofuVersion
libraryDependencies += "ru.tinkoff"     %% "tofu-zio-interop"            % tofuVersion
libraryDependencies += "ru.tinkoff"     %% "typed-schema-finagle-zio"    % tschemaVersion
libraryDependencies += "ru.tinkoff"     %% "typed-schema-finagle-custom" % tschemaVersion
libraryDependencies += "ru.tinkoff"     %% "typed-schema-swagger"        % tschemaVersion
libraryDependencies += "ru.tinkoff"     %% "typed-schema-swagger-ui"     % tschemaVersion
libraryDependencies += "com.github.julien-truffaut" %%  "monocle-core"   % monocleVersion
libraryDependencies += "com.github.julien-truffaut" %%  "monocle-macro"  % monocleVersion

libraryDependencies += scalaOrganization.value % "scala-reflect"         % scalaVersion.value
libraryDependencies += scalaOrganization.value % "scala-compiler"        % scalaVersion.value

addCompilerPlugin("com.olegpy"      %% "better-monadic-for" % "0.3.1")
addCompilerPlugin("org.typelevel"   %% "kind-projector" % "0.11.0" cross CrossVersion.patch)
addCompilerPlugin("org.wartremover" %% "wartremover" % "2.4.5" cross CrossVersion.full)


scalacOptions += "-P:wartremover:traverser:org.wartremover.warts.AsInstanceOf"
scalacOptions += "-P:wartremover:traverser:org.wartremover.warts.MutableDataStructures"
scalacOptions += "-P:wartremover:traverser:org.wartremover.warts.Null"
scalacOptions += "-P:wartremover:traverser:org.wartremover.warts.Return"
scalacOptions += "-P:wartremover:traverser:org.wartremover.warts.Throw"
scalacOptions += "-P:wartremover:traverser:org.wartremover.warts.Var"
scalacOptions += "-P:wartremover:traverser:org.wartremover.warts.While"
