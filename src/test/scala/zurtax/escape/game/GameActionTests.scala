package zurtax.escape.game

import java.time.OffsetDateTime

import org.scalatest._
import pureconfig.ConfigSource
import zurtax.escape.config.{ConfigMessageService, EscapeConfig}

class GameActionTests extends FlatSpec with Matchers {

  val difficulty: DifficultySettings = DifficultySettings()
  val id        = PlayerId("test")
  val player    = PlayerData(
    maze  = MazeId(1),
    hp    = difficulty.startingHealth,
    shots = 0,
    difficulty  = difficulty)
  val maze = MazeData(1)
  val tracing   = TracingData(maze)
  val timeline  = TimelineData(Vector(
    TimeUnit(0, 10, 10, "game started", "player is found himself in a maze")))
  val changes   = MazeChangesData()
  val src = MazePosition(Coords(), Tile)
  val dst = MazePosition(Coords(0, 1), Fence)

  val gameData = GameState(id, player, maze, tracing, timeline, changes, ApplyVector(src, South, dst))
  val config = ConfigSource.default.loadOrThrow[EscapeConfig]
  implicit  val messages = new ConfigMessageService(config.strings)


  import GameStateActions._

  val checker1 = movementWithoutFence     // fail, where is Fence
  val checker2 = gameNotSucceeded  // success, success not succeeded
  val checker3 = gameNotFailed    // success, not failed
  val checker4 = nutsArePresent   // fail, out of stock

  val andFastFailed = checker1 and checker2   // FAIL and SUCC  = FAIL
  val andSlowFailed = checker2 and checker1   // SUCC and FAIL  = FAIL
  val andTotalFails = checker1 and checker4   // FAIL and FAIL  = FAIL
  val andTotalPassd = checker2 and checker3   // SUCC and SUCC  = SUCC

  val orFastFailed = checker1 or checker2     // FAIL or SUCC   = SUCC
  val orSlowFailed = checker2 or checker1     // SUCC or FAIL   = SUCC
  val orTotalFails = checker1 or checker4     // FAIL or FAIL   = FAIL
  val orTotalPassd = checker2 or checker3     // SUCC or SUCC   = SUCC

  val allFastFailed = checker1 all checker2   // FAIL all SUCC  = SUCC
  val allSlowFailed = checker2 all checker1   // SUCC all FAIL  = FAIL
  val allTotalFails = checker1 all checker4   // FAIL all FAIL  = FAIL
  val allTotalPassd = checker2 all checker3   // SUCC all SUCC  = SUCC

  val anyFastFailed = checker1 both checker2  // FAIL any SUCC  = SUCC
  val anySlowFailed = checker2 both checker1  // SUCC any FAIL  = SUCC
  val anyTotalFails = checker1 both checker4  // FAIL any FAIL  = FAIL
  val anyTotalPassd = checker2 both checker3  // SUCC any SUCC  = SUCC

  "and" should "work" in {
    andTotalPassd.act(gameData) shouldBe Right(gameData)  // S1
    andSlowFailed.act(gameData).isLeft shouldBe true      // F2(+S1)
    andFastFailed.act(gameData).isLeft shouldBe true      // F1
    andTotalFails.act(gameData).isLeft shouldBe true      // F1
  }

  "or" should "work" in {
    orTotalPassd.act(gameData) shouldBe Right(gameData)   // S1
    orSlowFailed.act(gameData) shouldBe Right(gameData)   // S1
    orFastFailed.act(gameData).isLeft shouldBe false      // S2(+F1)
    orTotalFails.act(gameData).isLeft shouldBe true       // F2(+F1)
  }

  "all" should "work" in {
    allTotalPassd.act(gameData) shouldBe Right(gameData)  // S2(+S1)
    allSlowFailed.act(gameData).isLeft shouldBe true      // F2(+S1)
    allFastFailed.act(gameData).isLeft shouldBe false     // S2(+F1)
    allTotalFails.act(gameData).isLeft shouldBe true      // F2(+F1)
  }

  "both" should "work" in {
    anyTotalPassd.act(gameData) shouldBe Right(gameData)  // S2(+S1)
    anySlowFailed.act(gameData) shouldBe Right(gameData)  // S1
    anyFastFailed.act(gameData).isLeft shouldBe false     // S2(+F1)
    anyTotalFails.act(gameData).isLeft shouldBe true      // F2(+F1)
  }


}
