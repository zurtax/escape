package zurtax.escape.config

import zio.{URLayer, ZIO, ZLayer}
import zurtax.escape.{ConfigEnv, StringsEnv}

object MessageStrings {
  trait Service {
    def playerNotFound(name: String): String
    def mazeNotFound(number: Int): String
    def mazeAlreadyExists(number: Int): String
    def wrongDirection(direction: String): String
    def mazeLayoutBroken(): String
    def wallPosition(): String
    def fencePosition(): String
    def exitPosition(): String
    def outOfRange(): String
    def nutsAreOver(): String
    def ceilingNutThrow(): String
    def ceilingPlayerMove(): String
    def layoutParseSqare(): String
    def layoutParseBroken(): String
    def layoutDiameterBroken(): String
    def gameStopWall(): String
    def gameFailedMsg(): String
    def gameStarvedMsg(): String
    def gamePoisonedMsg(): String
    def gameImpactDeath(): String
    def gameOver(): String
    def gameStarted(): String
    def gamePrologue(): String
    def makeTimeUnit(): String
    def makeSuccess(): String
    def makeFailure(): String
    def makeOneStep(): String
    def makeNutCheck(): String
    def playerPlayer(): String
    def playerHp(): String
    def playerShots(): String
    def playerLoose(): String
    def playerStarved(): String
    def playerPoisoned(): String
    def playerScratched(): String
    def playerImpacted(): String
  }

  val live: URLayer[ConfigEnv, StringsEnv] =
    ZLayer.fromEffect(ZIO.access[ConfigEnv](config => new ConfigMessageService(config.get.strings)))

}

class ConfigMessageService(val config: StringsConfig) extends MessageStrings.Service {
  override def playerNotFound(name: String): String =
    s"${config.playerNotFoundPre} $name ${config.playerNotFoundPost}"

  override def mazeNotFound(number: Int): String =
    s"${config.mazeNotFoundPre} $number ${config.mazeNotFoundPost}"

  override def mazeAlreadyExists(number: Int): String =
    s"${config.mazeAlreadyExistsPre} $number ${config.mazeAlreadyExistsPost}"

  override def wrongDirection(direction: String): String =
    s"${config.wrongDirectionPre} $direction ${config.wrongDirectionPost}"

  override def mazeLayoutBroken(): String = config.mazeLayoutBroken

  override def wallPosition(): String = config.wallPosition

  override def fencePosition(): String = config.fencePosition

  override def exitPosition(): String = config.exitPosition

  override def outOfRange(): String = config.outOfRange

  override def nutsAreOver(): String = config.nutsAreOver

  override def ceilingNutThrow(): String = config.ceilingNutThrow

  override def ceilingPlayerMove(): String = config.ceilingPlayerMove

  override def layoutParseSqare(): String = config.layoutParseSqare

  override def layoutParseBroken(): String = config.layoutDiameterBroken

  override def layoutDiameterBroken(): String = config.layoutDiameterBroken

  override def gameStopWall(): String = config.gameStopWall

  override def gameFailedMsg(): String = config.gameFailedMsg

  override def gameStarvedMsg(): String = config.gameStarvedMsg

  override def gamePoisonedMsg(): String = config.gamePoisonedMsg

  override def gameImpactDeath(): String = config.gameImpactDeath

  override def gameOver(): String = config.gameOver

  override def gameStarted(): String = config.gameStarted

  override def gamePrologue(): String = config.gamePrologue

  override def makeOneStep(): String = config.makeOneStep

  override def makeNutCheck(): String = config.makeNutCheck

  override def makeSuccess(): String = config.makeSuccess

  override def makeFailure(): String = config.makeFailure

  override def makeTimeUnit(): String = config.makeTimeUnit

  override def playerPlayer(): String = config.playerPlayer
  override def playerHp(): String = config.playerHp
  override def playerShots(): String = config.playerShots
  override def playerLoose(): String = config.playerLoose

  override def playerStarved(): String = config.playerStarved
  override def playerPoisoned(): String = config.playerPoisoned
  override def playerScratched(): String = config.playerScratched
  override def playerImpacted(): String = config.playerImpacted

}
