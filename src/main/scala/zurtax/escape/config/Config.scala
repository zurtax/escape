package zurtax.escape
package config

import derevo.derive
import derevo.pureconfig.config
import pureconfig.ConfigSource
import zio.blocking.{Blocking, effectBlocking}
import zio.{RLayer, ZLayer}

@derive(config)
case class EscapeConfig(  db: DbConfig,
                          server: ServerConfig,
                          strings: StringsConfig )

@derive(config)
case class ServerConfig(  host: String,
                          port: Int )

@derive(config)
case class DbConfig(  url: String,
                      user: String,
                      password: String )

@derive(config)
case class StringsConfig( playerNotFoundPre: String,
                          playerNotFoundPost: String,
                          mazeNotFoundPre: String,
                          mazeNotFoundPost: String,
                          mazeAlreadyExistsPre: String,
                          mazeAlreadyExistsPost: String,
                          mazeLayoutBroken: String,
                          wrongDirectionPre:String,
                          wrongDirectionPost:String,
                          wallPosition: String,
                          fencePosition: String,
                          exitPosition: String,
                          outOfRange: String,
                          nutsAreOver: String,
                          ceilingNutThrow: String,
                          ceilingPlayerMove: String,
                          layoutParseSqare: String,
                          layoutParseBroken: String,
                          layoutDiameterBroken: String,
                          gameOver: String,
                          gameStopWall: String,
                          gameFailedMsg: String,
                          gameStarvedMsg: String,
                          gamePoisonedMsg: String,
                          gameImpactDeath: String,
                          gameStarted: String,
                          gamePrologue: String,
                          makeTimeUnit: String,
                          makeSuccess: String,
                          makeFailure: String,
                          makeOneStep: String,
                          makeNutCheck: String,
                          playerPlayer: String,
                          playerHp: String,
                          playerShots: String,
                          playerLoose: String,
                          playerStarved: String,
                          playerPoisoned: String,
                          playerScratched: String,
                          playerImpacted: String
                        )
/**
 * Контекст куска-конфигурации. Создается и можно добавлять к общему контексту.
 */
object EscapeConfig {
  val live: RLayer[Blocking, ConfigEnv] =
    ZLayer.fromEffect(effectBlocking(ConfigSource.default.loadOrThrow[EscapeConfig]))
}
