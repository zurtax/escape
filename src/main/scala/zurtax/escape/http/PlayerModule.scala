package zurtax.escape
package http

import com.twitter.finagle.http.Response
import ru.tinkoff.tschema.custom.syntax._
import ru.tinkoff.tschema.finagle.MkService
import ru.tinkoff.tschema.swagger.{MkSwagger, SwaggerBuilder}
import ru.tinkoff.tschema.syntax._
import zio.{RIO, ZIO}
import zurtax.escape.game.{AxisDirection, DifficultySettings, LogicalError, MazeCell, MazeId, PlayerData, PlayerId}

/**
 * Объект, который строит маршруты и связывает их с ф-циями из контекста, реализующие логику.
 */
object PlayerModule extends ApiModule {

  /**
   * маршруты
   */

  def startGame =
    opPost |> queryParam[MazeId]("maze") |>  jsonErr[ResponceError, Player]
  def startCustomGame =
    operation("custom")  |> queryParam[MazeId]("maze") |> queryParam[DifficultySettings]("difficulty") |>
      post |> jsonErr[ResponceError, Player]
  def getOne =
    opGet  |> jsonErr[ResponceError, PlayerData]
  def getMap =
    operation("automap")  |> get |> plainErr[ResponceError, String]
  def getLog =
    operation("timeline") |> queryParam[Option[String]]("sort") |> get |> jsonErr[ResponceError, Seq[String]]
  def checkCell =
    operation("check")  |> queryParam[String]("direction") |>
      put |> jsonErr[ResponceError, MazeCell]
  def nextStep =
    operation("move")   |> queryParam[String]("direction") |>
      put |> jsonErr[ResponceError, PlayerData]

  /**
   * склейка из всех обрабатываемых модулем маршрутов
   * @return общий корень player и далее одноранговый пока набор маршрутов
   */
  def api = tagPrefix("player") |> capture[PlayerId]("login") |>
    ( startGame <> startCustomGame <> getOne <> getMap <> getLog <> checkCell <> nextStep )

  /**
   * Объект с реализацией всех описаных выше функций-маршрутов
   * Куча одинаковых методов. Переписать на тайпклассы и комбинаторы
   */
  object handler {

    /**
     * Начало новой игры
     *
     * @param login кто играет
     * @param maze в каком лабиринте
     * @return новая игра, перетирает существующую
     */
    def post(login: PlayerId, maze: MazeId): Escape[Either[ResponceError, Player]] =
      RIO.access[LogicEnv](_.get).flatMap(_.createNewGame(login, maze, DifficultySettings()))

    /**
     * Начало новой игры с настройками сложности
     *
     * @param login кто играет
     * @param maze в каком лабиринте
     * @return новая игра, перетирает существующую
     */
    def custom(login: PlayerId, maze: MazeId, difficulty: DifficultySettings): Escape[Either[ResponceError, Player]] =
      RIO.access[LogicEnv](_.get).flatMap(_.createNewGame(login, maze, difficulty))

    /**
     * Resume started game
     *
     * @param login Для кого загружается игра
     * @return состояние игры
     */
    def get(login: PlayerId): Escape[Either[ResponceError, PlayerData]] =
      RIO.access[LogicEnv](_.get).flatMap(_.getPlayerStatus(login))

    /**
     * Карта разведанной части текущего уровня лабиринта
     * @param login для кого строим карту
     * @return карта местности
     */
    def automap(login: PlayerId): Escape[Either[ResponceError, String]] =
      RIO.access[LogicEnv](_.get).flatMap(_.getPlayerAutomap(login))

    /**
     * История приключений игрока
     * @param login чей журнал ищем
     * @return текстовая интерпретация
     */
    def timeline(login: PlayerId, sort: Option[String]): Escape[Either[ResponceError, Seq[String]]] =
      RIO.access[LogicEnv](_.get)
        .flatMap(l =>
          // без явного wrap идея имплисит видит, но компилировать отказывается
          wrap(l.getPlayerTimeline(login).map(secToReverse(sort)))
        )

    def secToReverse(sort: Option[String])(seq: Seq[String]): Seq[String] = sort match {
      case Some("desc") => seq.reverse
      case _ => seq
    }

    /**
     * Проверить болтом соседнюю ячейку
     *
     * @param login кто проверяет, которая игра
     * @param direction в каком направлении отправляется в полёт гайка.
     * @return пара из значения ячейки и нового состояния игры
     */
    def check(login: PlayerId, direction: String): Escape[Either[ResponceError, MazeCell]] =
      RIO.access[LogicEnv](_.get).flatMap { logic => AxisDirection(direction) match {
          case Right(axisDirection) => logic.checkMazeCell(login, axisDirection)
          case Left(_) => ZIO.succeed(Left(ResponceBadDirection(direction)))
      }
    }

    /**
     * Сделать один шаг
     *
     * @param login кто шагает
     * @param direction куда идём
     * @return результат
     */
    def move(login: PlayerId, direction: String): Escape[Either[ResponceError, PlayerData]] =
      RIO.access[LogicEnv](_.get).flatMap { logic => AxisDirection(direction) match {
          case Right(axisDirection) => logic.makeOneStep(login, axisDirection)
          case Left(_) => ZIO.succeed(Left(ResponceBadDirection(direction)))
        }
      }

  }

  /**
   * Сборка итоговой склейки маршрутов с логикой через макрос.
   * @return ZioRouting для нашего контекста
   */
  def route: EscapeRouting[Response] = MkService[EscapeRouting](api)(handler)

  /**
   * Сборка сваггера для нашего api
   * @return
   */
  def swag:  SwaggerBuilder = MkSwagger(api)

}

