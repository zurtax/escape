package zurtax.escape
package http

import cats.instances.list._
import cats.syntax.foldable._
import cats.syntax.semigroupk._
import com.twitter.finagle.http.Response
import com.twitter.finagle.http.filter.Cors
import com.twitter.finagle.{Http, ListeningServer}
import ru.tinkoff.tschema.finagle.zioRouting.HasRouting
import ru.tinkoff.tschema.finagle.{Routed, RunHttp}
import zio.console.putStrLn
import zio._
import zurtax.escape.config.ServerConfig


object EscapeServer {

  private val apiRoute: EscapeRouting[Response] =
    Routed
      .checkPrefix("/api", HttpModule.apiModules.foldMapK(_.route))
      .provideLayer(
        ZLayer.identity[EscapeEnv with HasRouting] +!+ RequestInfo.build
      )

  private val guiRoute: EscapeRouting[Response] =
    Routed
      .checkPrefix("/gui", HttpModule.guiModules.foldMapK(_.route))
      .provideLayer(
        ZLayer.identity[EscapeEnv with HasRouting] +!+ RequestInfo.build
      )

  private val route = /*guiRoute <+>*/ apiRoute <+> OpenApi.route <+> UI.route

  private val cors = new Cors.HttpFilter(Cors.UnsafePermissivePolicy)

  /**
   * Старт сервера теперь выглядит так.
   */
  private val start: Escape[ListeningServer] = for {
    // получаем контекст конфига и из него нужный конфиг
    ServerConfig(host, port) <- RIO.access[ConfigEnv](_.get.server)
    // здесь к серверу цепляются маршруты
    srv                      <- RunHttp.run[Escape](route.onError(e => putStrLn(e.untraced.prettyPrint)))
    full                     = cors.andThen(srv)
    // вешаем сервер на адрес и порт
    binding                  <- ZIO.effect(Http.serve(s"$host:$port", full))
  } yield binding

  private def report(srv: ListeningServer) =
    s"started at http:/${srv.boundAddress}/ see also http:/${srv.boundAddress}/${OpenApi.swaggerphp} for swagger-ui"

  /**
   * Сервер как ресурс - когда процесс, использующий сервер,
   * будет прерван - сервер будет остановлен
   */
  private val managedServer: RManaged[EscapeEnv, ListeningServer] =
    start.toManaged(s => UIO.effectAsync[Any](cb => s.close().respond(_ => cb(UIO.unit))))

  val run: URIO[SystemEnv, Any] =
    managedServer.use { srv => putStrLn(report(srv)) *> ZIO.never }
      .provideLayer(EscapeApp.live)
      .catchAll(error => putStrLn(error.toString))

}