package zurtax.escape.http

import java.util.Locale

import cats.instances.list._
import cats.syntax.foldable._
import cats.syntax.semigroupk._
import com.twitter.finagle.http.Response
import io.circe.Printer
import io.circe.syntax._
import ru.tinkoff.tschema.finagle.Routed
import ru.tinkoff.tschema.finagle.util.message
import ru.tinkoff.tschema.swagger
import ru.tinkoff.tschema.swagger.MkSwagger.PathSpec
import ru.tinkoff.tschema.swagger.{OpenApiInfo, PathDescription, SwaggerBuilder}
import zio._
import zurtax.escape.EscapeRouting

object OpenApi {

  private implicit val printer: Printer = Printer.spaces2.copy(dropNullValues = true)

  val swaggerjson: String = "swagger.json"
  val swaggerjars: String = "webjars"
  val swaggerphp:  String = "swagger-ui"

  private val swaggerHttp: EscapeRouting[Response] = {
    val response = message.stringResponse(swagger.SwaggerIndex(swaggerjson, swaggerjars))
    response.setContentType("text/html(UTF-8)")
    Routed.checkPath[EscapeRouting, Response](swaggerphp, ZIO.succeed(response))
  }

  private val swaggerResources: EscapeRouting[Response] = Resource.folder("/"+swaggerjars, "/META-INF/resources")

  val swaggerBuilder: SwaggerBuilder = HttpModule.apiModules.foldMap(_.swag).map(PathSpec.path.update(_, "api" +: _))

  private val swaggerJson: EscapeRouting[Response] =
    RIO.effectTotal {
      val descriptions =
        PathDescription.utf8I18n("swagger", Locale.forLanguageTag("ru"))
      val json = swaggerBuilder
        .describe(descriptions)
        .make(OpenApiInfo("fintech school", version = "0.0.1"))
        .asJson
        .printWith(printer)
      message.jsonResponse(json)
    }.flatMap(response => Routed.checkPath[EscapeRouting, Response](swaggerjson, ZIO.succeed(response)))

  val route: EscapeRouting[Response] = swaggerResources <+> swaggerHttp <+> swaggerJson
}
