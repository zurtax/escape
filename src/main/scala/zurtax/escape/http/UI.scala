package zurtax.escape.http

import cats.syntax.semigroupk._

object UI {
  import Resource.{single, folder}
  val route =
    single("/", "/ui/rules.html") <+>
      single("/rules.html", "/ui/rules.html") <+>
      single("/start.html", "/ui/start.html") <+>
      single("/game.html", "/ui/game.html") <+>
      folder("/js", "/ui", "text/javascript") <+>
      folder("/css", "/ui")
}
