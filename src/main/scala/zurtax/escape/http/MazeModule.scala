package zurtax.escape
package http

import com.twitter.finagle.http.Response
import derevo.circe.codec
import derevo.derive
import ru.tinkoff.tschema.custom.syntax._
import ru.tinkoff.tschema.finagle.MkService
import ru.tinkoff.tschema.param.HttpParam
import ru.tinkoff.tschema.swagger.{AsOpenApiParam, MkSwagger, Swagger, SwaggerBuilder}
import ru.tinkoff.tschema.syntax._
import zio.RIO
import zurtax.escape.game.MazeId


@derive(codec, Swagger, HttpParam, AsOpenApiParam)
case class LayoutParam(radius: Int, layout:String)

@derive(codec, Swagger)
case class MazeWithDesc(mazeNo: Int, mazeDesc: String)

object MazeModule extends ApiModule {

  def route: EscapeRouting[Response] = MkService[EscapeRouting](api.route)(handler)
  def swag:  SwaggerBuilder = MkSwagger(api.route)

  object api {
    def route =
      tagPrefix("maze") |> ( addAsString <> addAsPayload <> updAsString <> updAsPayload <> getAll )
    //---
    def addAsString =
      operation("addString")   |> post |> queryParam[LayoutParam]("layout") |> jsonErr[ResponceError, MazeId]
    def addAsPayload =
      operation("addLayout")   |> post |> jsonBody[LayoutParam]("layout") |> jsonErr[ResponceError, MazeId]
    def updAsString =
      capture[MazeId]("mazeNo") |>
        operation("updString") |> put  |> queryParam[LayoutParam]("layout") |> jsonErr[ResponceError, MazeId]
    def updAsPayload =
      capture[MazeId]("mazeNo") |>
        operation("updLayout") |> put  |> jsonBody[LayoutParam]("layout") |> jsonErr[ResponceError, MazeId]
    def getAll =
      operation("all") |> get |> json[Seq[MazeWithDesc]]
  }

  object handler {
    def addString(layout: LayoutParam): Escape[Either[ResponceError, MazeId]] =
      RIO.access[LogicEnv](_.get).flatMap(_.mazeAddString(layout.radius, layout.layout))

    def addLayout(layout: LayoutParam): Escape[Either[ResponceError, MazeId]] =
      RIO.access[LogicEnv](_.get).flatMap(_.mazeAddString(layout.radius, layout.layout))

    def updString(mazeNo: MazeId, layout: LayoutParam): Escape[Either[ResponceError, MazeId]] =
      RIO.access[LogicEnv](_.get).flatMap(_.mazeUpdString(mazeNo, layout.radius, layout.layout))

    def updLayout(mazeNo: MazeId, layout: LayoutParam): Escape[Either[ResponceError, MazeId]] =
      RIO.access[LogicEnv](_.get).flatMap(_.mazeUpdString(mazeNo, layout.radius, layout.layout))

    def all(): Escape[Seq[MazeWithDesc]] =
      RIO.access[LogicEnv](_.get).flatMap(_.all().map( _.map(p => MazeWithDesc(p._1, p._2))))
  }

}
