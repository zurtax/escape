package zurtax.escape.http

import java.time.Instant
import java.util.UUID
import java.util.concurrent.TimeUnit

import derevo.derive
import tofu.logging.Loggable
import tofu.logging.derivation.loggable
import zio.{UIO, URLayer, ZLayer}
import zio.clock._
import zurtax.escape.ReqInfo

@derive(loggable)
case class RequestInfo (
    traceId: UUID,
    startTime: Instant
)

/**
 * Журналирование запросов. Рассказывали на лекции. Лучше послушать там.
 */
object RequestInfo {
  implicit val uuidLoggable: Loggable[UUID] = Loggable[String].contramap(_.toString)

  val build: URLayer[Clock, ReqInfo] = ZLayer.fromEffect(
    for {
      traceId     <- UIO.effectTotal(UUID.randomUUID())
      startTimeMS <- currentTime(TimeUnit.MILLISECONDS)
    } yield RequestInfo(
      traceId = traceId,
      startTime = Instant.ofEpochMilli(startTimeMS),
    )
  )
}
