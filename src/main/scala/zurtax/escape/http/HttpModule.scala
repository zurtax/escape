package zurtax.escape
package http

import com.twitter.finagle.http.Response
import derevo.circe.codec
import derevo.derive
import ru.tinkoff.tschema.custom.AsResponse.Error
import ru.tinkoff.tschema.custom.derivation.jsonError
import ru.tinkoff.tschema.swagger.{Swagger, SwaggerBuilder, SwaggerContent}
import zurtax.escape.game.LogicalError


trait HttpModule {
  def route: EscapeRouting[Response]
}

trait ApiModule extends HttpModule {
  def swag: SwaggerBuilder

  /**
   * Единый метод обработки логических ошибок
   * @param call функция для оборачивания
   * @tparam T возвращаемый тип
   * @return ZIO с ошибкой готовой для ответа сервера
   */
  implicit def wrap[T](call: Escape[T]): Escape[Either[ResponceError, T]] =
    call.either.map(_.left.map( _ match {
      case c: LogicalError.PlayerNotFound    => ResponcePlayerNotFound(c.getMessage)
      case c: LogicalError.NutsAreOver       => ResponceCheckFailed(c.getMessage)
      case c: LogicalError.OutOfRange        => ResponceUnreachable(c.getMessage)
      case c: LogicalError.ExitPosition      => ResponceGameFinished(c.getMessage)
      case c: LogicalError.HealthLimitOut    => ResponceGameFinished(c.getMessage)
      case c: LogicalError.WallPosition      => ResponceMoveForbidden(c.getMessage)
      case c: LogicalError.FencePosition     => ResponceMoveForbidden(c.getMessage)
      case c: LogicalError.CeilingNutThrow   => ResponceCheckFailed(c.getMessage)
      case c: LogicalError.CeilingPlayerMove => ResponceMoveForbidden(c.getMessage)
      case c: LogicalError.MazeAlreadyExists => ResponceMazeNotFound(c.getMessage)
      case c: LogicalError.MazeLayoutBroken  => ResponceMazeNotFound(c.getMessage)
      case c: LogicalError.MazeNotFound      => ResponceMazeNotFound(c.getMessage)
    }))
}

trait GuiModule extends HttpModule

/**
 * Все варианты ошибок.
 */
@derive(SwaggerContent, Error)
sealed trait ResponceError

@derive(Swagger, codec, jsonError(410)) // reason: "You WIN!" or "You Loose."
final case class ResponceGameFinished(reason: String) extends ResponceError

@derive(Swagger, codec, jsonError(409))
final case class ResponceUnreachable(reason: String) extends ResponceError

@derive(Swagger, codec, jsonError(409))
final case class ResponceUnparceable(reason: String) extends ResponceError

@derive(Swagger, codec, jsonError(404))
final case class ResponcePlayerNotFound(playerName: String) extends ResponceError

@derive(Swagger, codec, jsonError(404))
final case class ResponceMazeNotFound(mazeId: String) extends ResponceError

@derive(Swagger, codec, jsonError(403))
final case class ResponceMoveForbidden(reason: String) extends ResponceError

@derive(Swagger, codec, jsonError(403))
final case class ResponceCheckFailed(reason: String) extends ResponceError

@derive(Swagger, codec, jsonError(400))
final case class ResponceBadDirection(direction: String) extends ResponceError

object HttpModule {
  val apiModules: List[ApiModule] = List(PlayerModule, MazeModule)
  val guiModules: List[GuiModule] = List()
}
