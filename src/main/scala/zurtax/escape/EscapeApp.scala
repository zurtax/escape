package zurtax.escape

import zio.{App, RLayer, ZIO, ZLayer}
import zurtax.escape.config.{EscapeConfig, MessageStrings}
import zurtax.escape.dao.ServiceContext
import zurtax.escape.game.{GameMechanics, LogicService}
import zurtax.escape.http.{EscapeServer, RequestInfo, ResourceCache}
import zurtax.escape.logging.{LoggedGameMechanics, LoggedServiceContext}


object EscapeApp extends App {
  /**
   * Здесь у нас ф-ция, которая "принимает на вход" системные штуки SystemEnv
   * и должна формировать на выходе полный контекст EscapeEnv
   * [[zurtax.escape.EscapeEnv]] - это LogicEnv + PlayerEnv + MazeEnv + SystemEnv
   * Для этого берутся аналогичные ф-ции и складываются
   */

  val pre: RLayer[SystemEnv, SystemEnv with ConfigEnv with StringsEnv] =
    (ZLayer.identity[SystemEnv] ++ EscapeConfig.live) >>>
      (ZLayer.identity[SystemEnv with ConfigEnv] ++ MessageStrings.live)

  val live: RLayer[SystemEnv, EscapeEnv] =
    pre >>> (ZLayer.identity[SystemEnv with ConfigEnv with StringsEnv] ++
      (ServiceContext.db >>> LoggedServiceContext.live) ++
      (LogicService.live >>> LoggedGameMechanics.live)  ++
      ResourceCache.live) ++ RequestInfo.build

  /**
   * Приложение ZIO для запуска ждет от нас ZIO[R,E,A], то есть ф-цию
   * Вот мы и даем ей подходящую ф-цию
   * @param args игнорируемые в данном случае аргументы
   * @return ф-ция для контекста по умолчанию, не предусматривающую исключений и с Int на выходе
   */
  def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    EscapeServer.run as 0
}
