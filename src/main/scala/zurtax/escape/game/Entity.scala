package zurtax.escape.game

import java.time.{Instant, OffsetDateTime, ZoneId}

import derevo.circe.codec
import derevo.derive
import ru.tinkoff.tschema.swagger.Swagger
import zio.clock.currentDateTime
import zurtax.escape.Escape


@derive(codec, Swagger)
final case class Entity[Id, Data](
        id: Id,
        data: Data,
        created: ActionInfo,
        modified: Option[ActionInfo] = None)

@derive(codec, Swagger)
final case class ActionInfo(date: OffsetDateTime, user: Option[String])

object Entities {
  def makeEntity[Id, Data](id: Id, data: Data): Escape[Entity[Id, Data]] =
     currentDateTime.map(dt => Entity(id, data, created = ActionInfo(dt, None)))

  def create[Id, Data](id: Id, data: Data): Entity[Id, Data] = {
    val dt = OffsetDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis), ZoneId.systemDefault)
    Entity(id, data, ActionInfo(dt, None))
  }
}
