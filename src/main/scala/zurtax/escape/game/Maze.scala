package zurtax.escape.game

import derevo.circe.codec
import derevo.derive
import ru.tinkoff.tschema.param.HttpParam
import ru.tinkoff.tschema.swagger.{AsOpenApiParam, Swagger}

import scala.util.Try

@derive(codec, Swagger)
case class MazeRow(row: Vector[MazeCell]) extends AnyVal

/** Этаж лабиринта */
@derive(codec, Swagger)
case class MazeFloor(floor: Vector[MazeRow]) extends AnyVal

@derive(codec, Swagger, HttpParam, AsOpenApiParam)
final case class MazeId(mazeNo:Int) extends AnyVal

/**
 * Лабиринт
 * @param layout карта лабиринта
 */
@derive(codec, Swagger)
final case class MazeData(layout: Vector[MazeFloor]) /*extends AnyVal*/ {
  import LogicalError._

  // считаем лабиринт кубическим
  lazy val radius: Int = (layout.head.floor.size-1) / 2
  // проверка лабиринта на планарность
  lazy val plain: Boolean = layout.size == 1

  /**
   * Что-то мне подсказывает, что так не стоит делать,
   * зато код гораздо более читаемый получается
   * @param coords координаты, которые мы проверяем на допустимость
   * @return true - если координаты не выходят за пределы лабиринта,
   *         false - в ином случае
   */
  def isInRange(coords:Coords):Boolean =
    (coords.x.abs <= radius) &&
      (coords.y.abs <= radius) &&
      (if (plain) coords.z == 0 else coords.z.abs <= radius)

  /** Получает ячейку, если возможно
    * @param z этаж
    * @param x запад-восток
    * @param y север-юг
    * @return [[scala.Some]] с ячейкой или [[scala.None]]
    */
  def apply(z: Int)(y: Int)(x: Int): Option[MazeCell] = // начало отсчета в центре (кроме этажа)
    Try(layout(if (plain) 0 else z + radius).floor(y + radius).row(x + radius)).toOption

  def apply(c: Coords): Option[MazeCell] =
    apply(c.z)(c.y)(c.x)

  // то же, что apply(c: Coords)
  /**
   * Все просто, если
   * @param coords координаты допустинмые, то
   * @return возвращаем ячейку, или ошибку в ином случае
   */
  def select(coords: Coords): Option[MazeCell] =
    if (isInRange(coords))
      Some(layout(if (plain) 0 else coords.z + radius)
      .floor(radius + coords.y)
      .row(radius + coords.x))
    else None

  def lookAt(cell: MazeCell): MazeCell =
    cell match {
      case Crate  => Wall
      case Poison => Tile
      case other  => other
    }

}

object MazeData {

  def apply(radius: Int): MazeData = MazeData(
    Vector(
      MazeFloor(
        Vector(MazeRow(row = Vector.fill(radius)(Fence) ++ Vector(Exit) ++ Vector.fill(radius)(Fence))) ++
          Vector.fill(radius * 2 - 1)(MazeRow(row = Fence +: Vector.fill(radius * 2 - 1)(Tile) :+ Fence)) ++
          Vector(MazeRow(row = Vector.fill(radius * 2 + 1)(Fence)))
      )
    )
  )

  def random(radius: Int): MazeData = MazeData(
    Vector(
      MazeFloor(
        Vector(MazeRow(row = Vector.fill(radius)(Fence) ++ Vector(Exit) ++ Vector.fill(radius)(Fence))) ++
          Vector.fill(radius - 1)(
            MazeRow(row = Fence +: Vector.fill(radius * 2 - 1)(MazeCell.rand) :+ Fence)
          ) ++
          Vector(
            MazeRow(row =
              Fence +: (
                Vector.fill(radius - 1)(MazeCell.rand)
                  ++ Vector(Tile)
                  ++ Vector.fill(radius - 1)(MazeCell.rand)
              ) :+ Fence
            )
          ) ++
          Vector.fill(radius - 1)(
            MazeRow(row = Fence +: Vector.fill(radius * 2 - 1)(MazeCell.rand) :+ Fence)
          ) ++
          Vector(MazeRow(row = Vector.fill(radius * 2 + 1)(Fence)))
      )
    )
  )
}
