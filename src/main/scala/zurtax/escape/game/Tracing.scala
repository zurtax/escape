package zurtax.escape.game

import derevo.circe.codec
import derevo.derive
import monocle.Lens
import monocle.macros.GenLens
import ru.tinkoff.tschema.swagger.Swagger
import zurtax.escape.Tracing

case class TracingId(playerId: PlayerId)

/**
 * Разведанная часть лабиринта.
 * Для удобства поиска разбита на этажи.
 * @param floors этажи
 * @param radius радиус лабиринта
 * @param plain true - если лабиринт одноэтажный, false - в противном случае
 * @param sized true - если границы лабиринта известны,
 *              false - если игрок пока не выяснил границ лабиринта.
 */
@derive(codec, Swagger)
case class TracingData(floors: Vector[Floor], radius: Int, plain: Boolean, sized: Boolean) {

  def toIndex(floor: Int): Int =
    if (plain) 0 else floor + radius
  def toFloor(position: Coords): FloorWithPosition =
    FloorWithPosition(floors(toIndex(position.z)), position)

  /**
   * Метание болта или гайки.
   * Если ячейка не разведана, то она разведывается и добавляется в трейсинг.
   * Если ячейка - Fence, значит границы лабиринта теперь известны, выставляется
   * соответствующий флаг и на всех этажах прорисовываются границы.
   * @param mazeData Структура лабиринта
   * @param changes Изменения лабиринта
   * @param coords Координаты разведываемой ячейки
   * @return Новый трейсинг, с точным типом разведанной ячейкой.
   */
  def revealCell(mazeData: MazeData, changes: MazeChangesData, coords: Coords): TracingData = {
    val floorIndex = toIndex(coords.z)
    changes.orElse(coords, mazeData(coords)).map {
      // именование вместе с "или" не работает, поэтому if
      case c: MazeCell if c == Fence || c == Exit => copy(
        sized = true,
        floors = floors.update(floorIndex, _ :+ Revealed(coords, c), !sized)
      )
      case c: MazeCell => copy(
        floors = floors.update(floorIndex, _ :+ Revealed(coords, c))
      )
    }
  }.getOrElse(this)

  /**
   * Если ячейка не разведана, то она разведывается и добавляется в трейсинг.
   * Разведываются и добавляются в трейсинг 4 неразведанные соседние ячейки.
   * Если одна из них - Fence, значит границы лабиринта теперь известны, выставляется
   * соответствующий флаг и на всех этажах прорисовываются границы.
   * @param mazeData Структура лабиринта
   * @param changes Изменения лабиринта
   * @param coords Координаты разведываемой ячейки
   * @return Новый трейсинг, с разведанной ячейкой.
   */
  def visitCell(mazeData: MazeData, changes: MazeChangesData = MazeChangesData(), coords: Coords = Coords()): TracingData =
    mazeData(coords) match {
      case Some(_) =>  // если координаты валидны
        val floorIndex = toIndex(coords.z)
        val floor = floors(floorIndex)
        // получаем и обновляем набор разведанных ячеек
        val revMap = floor.revealed.map(r => r.coords -> r.cell).toMap
        val updatedRevMap =
          (for {  // осматриваемся, получаем примерные типы ячеек
            n <- coords.neighbors.map(c => c -> changes.orElse(c, mazeData(c)))
            r <- n._2.map(n._1 -> mazeData.lookAt(_))
          } yield r)
            .foldLeft(revMap) { (acc, rev) =>
              val (c, cell) = rev
              if (acc.contains(c) && cell != Exit) acc  // второе условие на случай отображения стен до нахождения выхода
              else acc.updated(c, cell)
            } updated (coords, changes.orElse(coords, mazeData(coords)).get) // у ячейки с текущими координатами значение должно быть точное
        val updatedRev = updatedRevMap.toSeq.map{ case (key, value) => Revealed(key, value)}
        // Проверяем обновленный набор на наличие внешней стены
        val newSized = updatedRevMap.valuesIterator.contains(Fence) || updatedRevMap.valuesIterator.contains(Exit)
        // Обновляем (если значение sized поменялось, разведываем все внешние стены)
        copy(sized = newSized, floors = floors.update(
          floorIndex, _.copy( revealed = updatedRev ), !sized && newSized
        ))
      case None => this
    }

  implicit class FloorVectorOps(mazeTrace: Vector[Floor]) {
    /** Возвращает новый набор разведанных ячеек, заменяя в исходном этаж на преобразованный.
      * @param floorIndex индекс этажа
      * @param change функция преобразования этажа
      * @param shouldRevealFences нужно ли раскрыть границы лабиринта
      * @return новый набор разведанных ячеек
      */
    def update(floorIndex: Int, change: Floor => Floor,
               shouldRevealFences: Boolean = false ): Vector[Floor] = {
      val updatedFloors = mazeTrace.updated(floorIndex, change(mazeTrace(floorIndex)))
      if( shouldRevealFences )
        updatedFloors.revealBorders
      else updatedFloors
    }

    /** Добавляет разведанным ячейкам ячейки внешних стен и выход (в виде внешней стены).
      * @return новый набор разведанных ячеек
      */
    def revealBorders: Vector[Floor] = {
      // можно будет эту часть не менять, если сделать общий список открытых ячеек, а не разбитый по этажам
      val revealedFences = (-radius to (if (isPlain) -radius else radius))  // плоский лабиринт -> одно число
        .map( z => toIndex(z) -> Floor(
        (-radius until radius).iterator
          .flatMap( i => List((i,-radius), (radius, i), (-i,radius), (-radius, -i)))
          .map{ case (x,y) => Revealed(Coords(x,y, if( isPlain ) 0 else z), Fence) }.toSeq
      ))
      // обновляем
      revealedFences.foldLeft(mazeTrace){ (acc, value) =>
        val (index, floor) = value
        acc.updated(index, // если координаты выхода уже были в разведанных, их не обновляем
          acc(index) ++ (acc(index).revealed.find( r => r.cell == Exit ) match {
            case Some(r) => floor.revealed.filterNot( _.coords == r.coords )
            case None => floor.revealed
          }))
      }
    }

    def isPlain: Boolean = mazeTrace.size == 1
  }

}

/** Вспомогательный класс для маппинга через doobie */
case class TracingDataDB(radius: Int, plain: Boolean, sized: Boolean){
  val floors: Option[Vector[Floor]] = None
  def toTD(floors: Vector[Floor]): TracingData = {
    val allFloors =
      if(!plain && floors.size < 2*radius+1) {
        val floorsPresented = floors.filter( f => f.revealed.nonEmpty )
          .map( f => f.revealed.head.coords.z -> f).toMap
        (-radius to radius)
          .filterNot(floorsPresented.contains)
          .foldLeft(floorsPresented){(acc, i) => acc.updated(i, TracingData.emptyFloor)}
          .toVector.sortBy(_._1).map(_._2)
      }
      else floors
    TracingData(allFloors, radius, plain, sized)
  }
}

/**
 * Один этаж, насколько бы он ни был разведан.
 * @param revealed известная часть лабиринта
 */
@derive(codec, Swagger)
case class Floor(revealed: Seq[Revealed] = Vector.empty) {
  import TracingData.{append, concat}

  def ++(revealed: Seq[Revealed]): Floor = concat(this, revealed.toVector)

  def :+(revealed: Revealed): Floor = append(this, revealed)
}

// Вспомогательный класс для прорисовки
case class FloorWithPosition(floor: Floor, coords: Coords)
object FloorWithPosition {
  import cats.Show
  implicit val mazeFloorShow: Show[FloorWithPosition] =
    floor => Floor.showF(floor.floor, Some(floor.coords))
}

@derive(codec, Swagger)
case class Revealed(coords: Coords, cell: MazeCell)


object TracingData {

  /**
   * Построение нового трейса исходя из того, что известно о лабиринте.
   * Сразу прорисовывается страртовая позиция с соседними клетками.
   * @param mazeData Исходный лабиринт
   * @return Стартовый трейсинг( карта изведанной части лабиринта)
   */
  def apply(mazeData: MazeData): TracingData = TracingData(
    floors = if (mazeData.plain) Vector(startFloor)
    else Vector.fill(mazeData.radius)(emptyFloor) ++
      Vector(startFloor) ++
      Vector.fill(mazeData.radius)(emptyFloor),
    radius = mazeData.radius,
    plain = mazeData.plain,
    sized  = false
  ).visitCell(mazeData)

  val startFloor:Floor = Floor(Vector(Revealed(Coords(),Tile)))
  val emptyFloor:Floor = Floor(Vector.empty)

  def concat(floor: Floor, revealed: Vector[Revealed]): Floor =
    floor.copy(revealed = floor.revealed ++ revealed)

  def append(floor: Floor, revealed: Revealed): Floor =
    floor.copy(revealed = floor.revealed :+ revealed)

}

object Floor {
  import cats.Show

  implicit val mazeFloorShow: Show[Floor] = floor => showF(floor,None)

  def showF: (Floor, Option[Coords]) => String = (floor, playerCoords) => {
    def max(l: Int, r: Int) = if (l > r) l else r
    def repRow(automap: Vector[String], index: Int, string: String): Vector[String] =
      automap.take(index) ++ Vector(string) ++ automap.takeRight(automap.size-index-1)
    def repCol(string: String, index: Int, char: Char): String =
      string.substring(0, index) + char + string.substring(index+1)
    def repCell(automap: Vector[String], rowIndex: Int, colIndex: Int, char: Char): Vector[String] =
      repRow(automap, rowIndex, repCol(automap(rowIndex), colIndex, char))
    val radius = floor.revealed.foldLeft(0) {
      (acc,rev) => max(acc, max(rev.coords.x.abs, rev.coords.y.abs))
    }
    val empty:Vector[String] = Vector.fill(radius*2+1)("░" * (radius*2+1))
    val entrypoint: Coords => Boolean = _==Coords()
    val playerpoint: Coords => Boolean = c => playerCoords.contains(c)
    val filled = floor.revealed.foldLeft(empty) {
      (automap,rev) =>
        val rx = rev.coords.x+radius
        val ry = rev.coords.y+radius
        if ( playerpoint(rev.coords) ) repCell(automap, ry, rx, '@')
        else if( entrypoint(rev.coords) ) repCell(automap, ry, rx, '°')
        else rev.cell match {
          case Tile      => repCell(automap, ry, rx, rev.cell.print)
          case Poison    => repCell(automap, ry, rx, rev.cell.print)
          case Wall      => repCell(automap, ry, rx, rev.cell.print)
          case Crate     => repCell(automap, ry, rx, rev.cell.print)
          case Exit      => repCell(automap, ry, rx, rev.cell.print)
          case Stairs(_) => repCell(automap, ry, rx, rev.cell.print)
          case Fence  => (rev.coords.y, rev.coords.x) match {
            case (row,col) if row ==  radius && col ==  radius =>
              repCell(automap, row+radius, col+radius, '╝')
            case (row,col) if row ==  radius && col == -radius =>
              repCell(automap, row+radius, col+radius, '╚')
            case (row,col) if row == -radius && col ==  radius =>
              repCell(automap, row+radius, col+radius, '╗')
            case (row,col) if row == -radius && col == -radius =>
              repCell(automap, row+radius, col+radius, '╔')
            case (row,col) if row == -radius || row ==  radius =>
              repCell(automap, row+radius, col+radius, '═')
            case (row,col) if col == -radius || col ==  radius =>
              repCell(automap, row+radius, col+radius, '║')
          }
        }
    }
    filled.mkString("\n")
  }
}

object TracingOps {
  val tracingData: Lens[Tracing, TracingData] = GenLens[Tracing](_.data)
}