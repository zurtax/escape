package zurtax.escape.game

import derevo.circe.codec
import derevo.derive
import ru.tinkoff.tschema.swagger.Swagger


sealed trait Direction

sealed trait Forward extends Direction
sealed trait Backward extends Direction

sealed trait Axis
sealed trait Horizontal
sealed trait Vertical

sealed trait X extends Axis with Horizontal
sealed trait Y extends Axis with Horizontal
sealed trait Z extends Axis with Vertical

@derive(codec, Swagger)
sealed trait AxisDirection extends Direction with Axis

// Оси Оx и Оy удобнее отсчитывать - индекс (0,0) - слева-направо и сверху-вниз
// При этом центр лабиринта (место появление игрока) ничто не мешает взять за точку отсчета
@derive(codec, Swagger)
object North extends AxisDirection with Y with Backward // север = "вверх", уменьшение индекса
@derive(codec, Swagger)
object South extends AxisDirection with Y with Forward
@derive(codec, Swagger)
object East extends AxisDirection  with X with Forward
@derive(codec, Swagger)
object West extends AxisDirection  with X with Backward
@derive(codec, Swagger)
object Up extends AxisDirection    with Z with Forward
@derive(codec, Swagger)
object Down extends AxisDirection  with Z with Backward


object AxisDirection {
  import Coords._
  import LogicalError._
  import cats.Eq
  import cats.Show
  import cats.syntax.eq._
  import cats.syntax.show._
  import cats.instances.string._

  def step(dir: AxisDirection)(src: Int): Int = dir match {
    case _:Forward => src+1
    case _:Backward => src-1
  }

  def applyTo(dir: AxisDirection)(coords: Coords): Coords  = dir match {
    case _:X => coordX.modify(step(dir))(coords)
    case _:Y => coordY.modify(step(dir))(coords)
    case _:Z => coordZ.modify(step(dir))(coords)
  }

  /**
    * Возвращает значение направления по имени (без учета регистра) или ничего.
    * @param name имя направления
    * @return [[scala.Some]] со значением направления по имени или [[scala.None]]
    */
  def apply(name: String): Either[LogicalError, AxisDirection] = name.toLowerCase match {
    case "north" => Right(North)
    case "south" => Right(South)
    case "east"  => Right(East)
    case "west"  => Right(West)
    case "up"    => Right(Up)
    case "down"  => Right(Down)
    case error   => Left(WrongDirection(error))
  }

  implicit val directionSimpleName: Show[AxisDirection] = {
    case North => "North"
    case South => "South"
    case East  => "East"
    case West  => "West"
    case Up    => "Up"
    case Down  => "Down"
  }

  implicit val mazeCellEqual: Eq[AxisDirection] =
    Eq.instance[AxisDirection] { (dir1, dir2) => dir1.show === dir2.show }
}
