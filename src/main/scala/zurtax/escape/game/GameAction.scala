package zurtax.escape.game

trait GameAction[F[_, _], E, S] {
  def act(state: S): F[E, S]
}

trait GameActionOps[F[_, _], E, S] {

  /**
   * реализует аналог first.act(state).flatMap(second.act(_))
   * Второй GameAction будет запущен, если первый выполнился с успехом
   * S1 => S2 == S2
   * F1 => S2 == F1
   * S1 => F2 == F2
   * F1 => F2 == F1
   * @param first GameAction
   * @param second GameAction
   * @return провал первого или результат второго
   */
  def and(first: GameAction[F, E, S], second: GameAction[F, E, S]): GameAction[F, E, S]

  /**
   * реализует аналог возвращает first.act(state).recoverWith(e => second.act(recover(e)))
   * Второй GameAction будет запущен, если первый провалился
   * S1 => S2 == S1
   * F1 => S2 == S2
   * S1 => F2 == S1
   * F1 => F2 == F2
   * @param first GameAction
   * @param second GameAction
   * @return успех первого или второго, иначе провал второго
   */
  def or(first: GameAction[F, E, S], second: GameAction[F, E, S]): GameAction[F, E, S]

  /**
   * реализует аналог first.act(state).flatMap(second.act(_)).recoverWith(e => second.act(recover(e)))
   * будет возвращен результат второго, независимо от результата первого
   * S1 => S2 == S2
   * F1 => S2 == S2
   * S1 => F2 == F2
   * F1 => F2 == F2
   * @param first GameAction
   * @param second GameAction
   * @return результат второго
   */
  def all(first: GameAction[F, E, S], second: GameAction[F, E, S]): GameAction[F, E, S]

  /**
   * Выполнится первый, потом второй.
   * Если результат второго провал, то вернется результат первого.
   * Если оба завершились провалом, то вернется провал второго.
   * S1 => S2 == S2
   * F1 => S2 == S2
   * S1 => F2 == S1
   * F1 => F2 == F2
   * @param first GameAction
   * @param second GameAction
   * @return успех второго, или успех первого, если провалился второй, или провал второго, если провалились оба
   */
  def both(first: GameAction[F, E, S], second: GameAction[F, E, S]): GameAction[F, E, S]

  /**
   * выполняется первый, если провал - выполняется левый, если успех - правый
   * @param first решающий GameAction
   * @param left  ветка в случае провала по решению
   * @param right ветка в случае успеха по решению
   */
  def fork(first: GameAction[F, E, S], left: GameAction[F, E, S], right: GameAction[F, E, S]): GameAction[F, E, S]

  /**
   * Если у ошибки есть копия состояния, получить эту копию
   */
  val recover: PartialFunction[E, S]
}

object GameAction {

  implicit class GameActionAux[F[_, _], E, S](val action: GameAction[F, E, S]) {
    def and(second: GameAction[F, E, S])(implicit mapper: GameActionOps[F, E, S]): GameAction[F, E, S] =
      mapper.and(action, second)
    def or(second: GameAction[F, E, S])(implicit mapper: GameActionOps[F, E, S]): GameAction[F, E, S] =
      mapper.or(action, second)
    def all(second: GameAction[F, E, S])(implicit mapper: GameActionOps[F, E, S]): GameAction[F, E, S] =
      mapper.all(action, second)
    def both(second: GameAction[F, E, S])(implicit mapper: GameActionOps[F, E, S]): GameAction[F, E, S] =
      mapper.both(action, second)
    def fork(left: GameAction[F, E, S], right: GameAction[F, E, S])(implicit mapper: GameActionOps[F, E, S]): GameAction[F, E, S] =
      mapper.fork(action, left, right)
  }

  implicit def seqToAct[F[_, _], E, S](seq: Seq[GameAction[F,E,S]])(implicit mapper: GameActionOps[F, E, S]): GameAction[F,E,S] =
    reduce(seq)

  // до первого Failure
  def reduce[F[_, _], E, S](seq: Seq[GameAction[F,E,S]])(implicit mapper: GameActionOps[F, E, S]): GameAction[F,E,S] =
    seq match {
      case head::Nil => head
      case head::tail => tail.foldLeft(head) { (acc, next) => acc and next }
    }

  // выполнить всю цепочку
  def reduceAll[F[_, _], E, S](seq: Seq[GameAction[F,E,S]])(implicit mapper: GameActionOps[F, E, S]): GameAction[F,E,S] =
    seq match {
      case head::Nil => head
      case head::tail => tail.foldLeft(head) { (acc, next) => acc all next }
    }

  // до первого Success
  def reduceAny[F[_, _], E, S](seq: Seq[GameAction[F,E,S]])(implicit mapper: GameActionOps[F, E, S]): GameAction[F,E,S] =
    seq match {
      case head::Nil => head
      case head::tail => tail.foldLeft(head) { (acc, next) => acc or next }
    }
}

/**
 * Проверки не меняют состояние, если был "успех".
 * Если был "провал", то возващается ошибка.
 * Если ошибка может хранить состояние, то сохраняется измененное состояние.
 * @tparam F контейнер
 * @tparam E тип ошибки
 * @tparam S тип состояния
 */
abstract class Verifier[F[_, _], E, S] extends GameAction[F, E, S] {
  val verify: PartialFunction[S, E]
  val success: S => F[E, S]
  val failure: E => F[E, S]
  override def act(state: S): F[E, S] = if (verify.isDefinedAt(state)) failure(verify(state)) else success(state)
}

/**
 * Функция меняет состояние. В любом случае возвращается "успех" с состоянием, измененным или нет.
 * @tparam F контейнер
 * @tparam E тип ошибки
 * @tparam S тип состояния
 */
abstract class Transformer[F[_, _], E, S] extends GameAction[F, E, S] {
  val transform: PartialFunction[S, S]
  val success: S => F[E, S]
  override def act(state: S): F[E, S] = if (transform.isDefinedAt(state)) success(transform(state)) else success(state)
}
