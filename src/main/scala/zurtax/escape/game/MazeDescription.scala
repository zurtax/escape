package zurtax.escape.game

import derevo.circe.codec
import derevo.derive
import ru.tinkoff.tschema.swagger.Swagger


@derive(codec, Swagger)
case class MazeDescriptionId (mazeId: MazeId)

@derive(codec, Swagger)
case class MazeDescriptionData (description: String)
