package zurtax.escape.game

import derevo.circe.codec
import derevo.derive
import doobie.util.meta.Meta
import monocle.Lens
import monocle.macros.GenLens
import ru.tinkoff.tschema.param.HttpParam
import ru.tinkoff.tschema.swagger.{AsOpenApiParam, Swagger}
import zurtax.escape.{Health, Player, Shots, Steps}


@derive(codec, Swagger, HttpParam, AsOpenApiParam)
final case class DifficultySettings(
                        wallDamage  : Health =  1,
                        crateDamage : Health =  1,
                        poisonDamage: Health =  1,
                        hungerDamage: Health =  1,
                        hungerPeriod: Steps  = 10,
                        startingHealth:Health= 10,
                        startingShots: Shots = 10)

object DifficultySettings {
  private val regex = raw"(\-?\d+),(\-?\d+),(\-?\d+),(\-?\d+),(\-?\d+),(\-?\d+),(\-?\d+)".r

  private val to: DifficultySettings => String =
    d => s"${d.wallDamage},${d.crateDamage},${d.poisonDamage},${d.hungerDamage},${d.hungerPeriod},${d.startingHealth},${d.startingShots}"

  private val from: String => DifficultySettings = {
    case regex(w, c, p, hd, hp, sh, ss) => DifficultySettings(w.toInt, c.toInt, p.toInt, hd.toInt, hp.toInt, sh.toInt, ss.toInt)
  }

  implicit val diffDBMeta: Meta[DifficultySettings] = Meta[String].timap(from)(to)
}


@derive(codec, Swagger, HttpParam, AsOpenApiParam)
case class PlayerId(login: String) extends AnyVal

@derive(codec, Swagger)
case class PlayerData ( maze      : MazeId,
                        hp        : Health,
                        shots     : Shots,
                        difficulty: DifficultySettings,
                        coords    : Coords = Coords(),
                        steps     : Steps  = 0 )

object PlayerOps {

  val playerData  : Lens[Player, PlayerData] = GenLens[Player](_.data)
  val idLogin     : Lens[PlayerId, String]   = GenLens[PlayerId](_.login)
  val dataShots   : Lens[PlayerData, Shots]  = GenLens[PlayerData](_.shots)
  val dataHealth  : Lens[PlayerData, Health] = GenLens[PlayerData](_.hp)
  val dataCoords  : Lens[PlayerData, Coords] = GenLens[PlayerData](_.coords)
  val dataSteps   : Lens[PlayerData,Steps]   = GenLens[PlayerData](_.steps)
  val dataDiffic  : Lens[PlayerData,DifficultySettings]= GenLens[PlayerData](_.difficulty)
  val playerShots : Lens[Player, Shots]      = playerData composeLens dataShots
  val playerHealth: Lens[Player, Health]     = playerData composeLens dataHealth
  val playerCoords: Lens[Player, Coords]     = playerData composeLens dataCoords
  val playerSteps : Lens[Player,Steps]       = playerData composeLens dataSteps
  val playerDiffic: Lens[Player,DifficultySettings]= playerData composeLens dataDiffic

}