package zurtax.escape.game

import cats.syntax.show._
import monocle.Lens
import monocle.macros.GenLens
import zurtax.escape.config.MessageStrings
import zurtax.escape.{Health, Shots, Steps, Timeline}


case class TimeUnit(step: Steps, hp: Health, shots: Shots, action: String, events: String)

object TimeUnit {
  def apply(playerData: PlayerData, action: String, events: String): TimeUnit = TimeUnit(
    step   = playerData.steps,
    hp     = playerData.hp,
    shots  = playerData.shots,
    action = action,
    events = events)
}

case class TimelineId(playerId: PlayerId)
case class TimelineData(frames: Vector[TimeUnit])

object TimelineData {
  import PlayerOps._
  implicit val timelineDataToOps: TimelineData => TimelineDataOps = new TimelineDataOps(_)

  def apply(tu: TimeUnit): TimelineData = TimelineData(Vector(tu))

  val timelineId    : Lens[Timeline, TimelineId]  = GenLens[Timeline](_.id)
  val idPlayerId    : Lens[TimelineId, PlayerId]  = GenLens[TimelineId](_.playerId)
  val timeIdLogin   : Lens[TimelineId, String]    = idPlayerId composeLens idLogin
  val timelineLogin : Lens[Timeline, String]      = timelineId composeLens timeIdLogin

}

class TimelineDataOps(timeline: TimelineData) {
  import TimelineDataOps._

  def gameover(playerData: PlayerData)(
    implicit config: MessageStrings.Service): TimelineData =
    dataFrames.modify(_ appended TimeUnit(
      playerData = playerData,
      action     = s"${config.gameOver()}:",
      events     = config.gameFailedMsg
    ))(timeline)

  def visit(  playerData: PlayerData, direction: AxisDirection, failure: Option[String] = None)(
              implicit config: MessageStrings.Service): TimelineData =
    dataFrames.modify(_ appended TimeUnit(
      playerData = playerData,
      action     = s"${config.makeOneStep} ${direction.show}",
      events     = failure.map(f => s"${config.makeFailure}  $f")
        .getOrElse(config.makeSuccess)
    ))(timeline)

  def check(  playerData: PlayerData, direction: AxisDirection, failure: Option[String] = None)(
              implicit config: MessageStrings.Service): TimelineData =
    dataFrames.modify(_ appended TimeUnit(
      playerData = playerData,
      action     = s"${config.makeNutCheck} ${direction.show}",
      events     = failure.map(f => s"${config.makeFailure}  $f")
        .getOrElse(config.makeSuccess)
    ))(timeline)

  def starve( playerData: PlayerData)(
              implicit config: MessageStrings.Service): TimelineData =
    dataFrames.modify(_ appended TimeUnit(
      playerData = playerData,
      action     = s"${config.playerLoose} ${playerData.difficulty.hungerDamage}${config.playerHp}",
      events     = config.playerStarved
    ))(timeline)

  def poisoned( playerData: PlayerData)(
                implicit config: MessageStrings.Service): TimelineData =
    dataFrames.modify(_ appended TimeUnit(
      playerData = playerData,
      action     = s"${config.playerLoose} ${playerData.difficulty.poisonDamage}${config.playerHp}",
      events     = config.playerPoisoned
    ))(timeline)

  def scratch(  playerData: PlayerData)(
    implicit config: MessageStrings.Service): TimelineData =
    dataFrames.modify(_ appended TimeUnit(
      playerData = playerData,
      action     = s"${config.playerLoose} ${playerData.difficulty.crateDamage}${config.playerHp}",
      events     = config.playerScratched
    ))(timeline)

  def impact(  playerData: PlayerData)(
    implicit config: MessageStrings.Service): TimelineData =
    dataFrames.modify(_ appended TimeUnit(
      playerData = playerData,
      action     = s"${config.playerLoose} ${playerData.difficulty.wallDamage}${config.playerHp}",
      events     = config.playerImpacted
    ))(timeline)

  def poisonedToDeath(playerData: PlayerData)(
    implicit config: MessageStrings.Service): TimelineData =
    dataFrames.modify(_ appended TimeUnit(
      playerData = playerData,
      action     = s"${config.gameOver()}:",
      events     = config.gamePoisonedMsg
    ))(timeline)

  def starvedToDeath(playerData: PlayerData)(
    implicit config: MessageStrings.Service): TimelineData =
    dataFrames.modify(_ appended TimeUnit(
      playerData = playerData,
      action     = s"${config.gameOver()}:",
      events     = config.gameStarvedMsg
    ))(timeline)

  def impactToDeath(playerData: PlayerData)(
    implicit config: MessageStrings.Service): TimelineData =
    dataFrames.modify(_ appended TimeUnit(
      playerData = playerData,
      action     = s"${config.gameOver()}:",
      events     = config.gameImpactDeath
    ))(timeline)

  def exitPointReached(playerData: PlayerData)(
    implicit config: MessageStrings.Service): TimelineData =
    dataFrames.modify(_ appended TimeUnit(
      playerData = playerData,
      action     = s"${config.gameOver()}:",
      events     = config.exitPosition
    ))(timeline)

  def translate()(implicit config: MessageStrings.Service): Seq[String] =
    timeline.frames.map(frame =>
      s"${config.playerPlayer}[${config.playerHp}:${frame.hp},${config.playerShots}:${frame.shots}] ${config.makeTimeUnit} ${frame.step} ${frame.action} ${frame.events}" )
}


object TimelineDataOps {

  import monocle.Lens
  import monocle.macros.GenLens

  implicit val timelineData  : Lens[Timeline, TimelineData]          = GenLens[Timeline](_.data)
  implicit val dataFrames    : Lens[TimelineData, Vector[TimeUnit]]  = GenLens[TimelineData](_.frames)
  implicit val timelineFrames: Lens[Timeline, Vector[TimeUnit]]      = timelineData composeLens dataFrames


}