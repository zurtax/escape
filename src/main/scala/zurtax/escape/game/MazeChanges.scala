package zurtax.escape.game

import derevo.circe.codec
import derevo.derive
import monocle.Lens
import monocle.macros.GenLens
import ru.tinkoff.tschema.swagger.Swagger
import zurtax.escape.MazeChanges

case class MazeChangesId(playerId: PlayerId)

/** Изменения ячеек лабиринта (типа, наличия болтов и т.д.).
  * Реализация методов подразумевает наличие не более одного изменения каждого типа в списке.
  */
@derive(codec, Swagger)
case class MazeChangesData(changes: Seq[MazeChange] = Vector.empty, bolts: Seq[ShotsAmount] = Vector.empty) {
  def changed(coords: Coords): Boolean =
    changes.exists(c => c.coords == coords)

  def getOrElse(coords: Coords, default: MazeCell): MazeCell =
    changes.find(_.coords == coords).map(_.cell).getOrElse(default)

  def orElse(coords: Coords, default: Option[MazeCell]): Option[MazeCell] =
    changes.find(_.coords == coords).map(_.cell).orElse(default)

  def :+(coords: Coords, mazeCell: MazeCell): MazeChangesData =
    MazeChangesData(changes :+ MazeChange(coords, mazeCell), bolts)

  def getOrElse(coords: Coords, default: Int = 0): Int =
    bolts.find(_.coords == coords).map(_.amount).getOrElse(default)

  def replaced(coords: Coords, newShotsAmount: Int): MazeChangesData =
    MazeChangesData(changes, bolts.takeWhile(_.coords != coords)
      ++ bolts.dropWhile(_.coords != coords).drop(1)
      :+ ShotsAmount(coords, newShotsAmount))

  def updated(coords: Coords, shotsAmountChange: Int): MazeChangesData =
    MazeChangesData(changes, bolts.takeWhile(_.coords != coords)
      ++ bolts.dropWhile(_.coords != coords).drop(1)
      :+ ShotsAmount(coords, getOrElse(coords) + shotsAmountChange))

  def removeShots(coords: Coords): (MazeChangesData, Int) =
    (MazeChangesData(changes, bolts.takeWhile(_.coords != coords)
      ++ bolts.dropWhile(_.coords != coords).drop(1)), getOrElse(coords))
}

@derive(codec, Swagger)
case class MazeChange(coords: Coords, cell: MazeCell)

@derive(codec, Swagger)
case class ShotsAmount(coords: Coords, amount: Int)

object MazeChangesOps {
  val mazeChangesData: Lens[MazeChanges, MazeChangesData] = GenLens[MazeChanges](_.data)
}
