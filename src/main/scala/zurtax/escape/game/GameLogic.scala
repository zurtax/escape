package zurtax.escape.game

import cats.Monad
import cats.data.EitherT
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.syntax.show._
import zurtax.escape.config.MessageStrings
import zurtax.escape.dao.{MazeDescriptions, Mazes, MazesChanges, Players, Timelines, Tracings}
import zurtax.escape.game.LogicalError.{mazeLayoutBroken, mazeNotFound, outOfRange, playerNotFound}

object PlayerLogic {
  import GameStateActions._

  /**
   * Для указанного игрока создается новая игра.
   *
   * @param playerId    - игрок, для которога начинаем игру
   * @param mazeId      - лабиинт, в котором игрок хочет начать игру
   * @param difficulty  - настройки сложности
   * @param players     - механизм получения / сохранения данных
   * @param mazes       - механизм получения / сохранения данных
   * @param tracings    - механизм получения / сохранения данных
   * @param timelines   - механизм получения / сохранения данных
   * @param mazechanges - механизм получения / сохранения данных
   * @param messages    - механизм получения / сохранения данных
   * @tparam F          - контейнер
   * @return            - если нет желаемого лабиринта, тогда ошибка, иначе новое состояние игры
   */
  def createNewGame[F[_] : Monad](
                          playerId: PlayerId,
                          mazeId:   MazeId,
                          difficulty: DifficultySettings)
                         (implicit  players: Players.Accessor[F],
                                    mazes: Mazes.Accessor[F],
                                    tracings: Tracings.Accessor[F],
                                    timelines: Timelines.Accessor[F],
                                    mazechanges: MazesChanges.Accessor[F],
                                    messages: MessageStrings.Service)
                          : F[Either[LogicalError, PlayerData]] = {
    val mdF = mazes .getOne(mazeId) .flatMap {
        case Some(mazeData) =>
          val playerData = PlayerData(mazeId, difficulty.startingHealth, difficulty.startingShots, difficulty)
          val tlData = TimelineData( TimeUnit(playerData, messages.gameStarted(), messages.gamePrologue()) )
          val r: F[Option[PlayerData]] = for {
            id <- players.createOrUpdate(playerId, playerData.copy(steps = playerData.steps+1))
            _  <- tracings.createOrUpdate(TracingId(playerId), TracingData(mazeData))
            _  <- timelines.createOrUpdate(TimelineId(playerId), tlData)
            _  <- mazechanges.createOrUpdate(MazeChangesId(playerId), MazeChangesData())
            dt <- players.getOne(id)
          } yield dt
          r
        case None => val l: F[Option[PlayerData]] = implicitly[Monad[F]].pure(None); l
      }
    mdF.map( _.toRight(mazeNotFound(mazeId.mazeNo)) )
  }

  def getPlayerStatus[F[_] : Monad](playerId: PlayerId)
                           (implicit  players: Players.Accessor[F],
                                      messages: MessageStrings.Service)
                          : F[Either[LogicalError, PlayerData]] =
    players.getOne(playerId).map(_.toRight(playerNotFound(playerId.login)))

  def getPlayerAutomap[F[_] : Monad](playerId: PlayerId)
                                    (implicit players: Players.Accessor[F],
                                     tracings: Tracings.Accessor[F],
                                              messages: MessageStrings.Service)
                          : F[Either[LogicalError, String]] = {
    val playerFE  = players.getOne(playerId).map(_.toRight(playerNotFound(playerId.login)))
    val tracingFE = tracings.getOne(TracingId(playerId)).map(_.toRight(playerNotFound(playerId.login)))
    playerFE.flatMap(playerE => tracingFE.map(tracingE =>
      playerE.flatMap(data => tracingE.map(tracing =>
        tracing.toFloor(data.coords).show))
    ))
  }

  def getPlayerTimeline[F[_] : Monad](playerId: PlayerId)
                                     (implicit players: Players.Accessor[F],
                                      timelines: Timelines.Accessor[F],
                                      messages: MessageStrings.Service)
                          : F[Either[LogicalError, Seq[String]]] =
    timelines.getOne(TimelineId(playerId)).map(_.map(_.translate()).toRight(playerNotFound(playerId.login)))

  def checkMazeCell[F[_] : Monad](playerId: PlayerId,
                                  direction: AxisDirection)
                                   (implicit  players: Players.Accessor[F],
                                    mazes: Mazes.Accessor[F],
                                    tracings: Tracings.Accessor[F],
                                    timelines: Timelines.Accessor[F],
                                    mazechanges: MazesChanges.Accessor[F],
                                    messages: MessageStrings.Service,
                                    dices: DiceSet.Service)
                                  : F[Either[LogicalError, MazeCell]] =
    for {
      extracted   <- extractFor(playerId, direction)
      transformed  = extracted.flatMap(
        Seq(gameNotSucceeded, gameNotFailed, damageFromPoison, poisonedButAlive,
          damageFromHunger, starvedButAlive, nutsArePresent,
          checkFlatThrowing, doThrowNut, fixThisStep) .act(_) )
      persisted   <- persistState(transformed)
    } yield persisted.map( _.vector.dst.cell )

  def makeOneStep[F[_] : Monad](playerId: PlayerId,
                                direction: AxisDirection)
                               (implicit  players: Players.Accessor[F],
                                mazes: Mazes.Accessor[F],
                                tracings: Tracings.Accessor[F],
                                timelines: Timelines.Accessor[F],
                                mazechanges: MazesChanges.Accessor[F],
                                messages: MessageStrings.Service,
                                dices: DiceSet.Service)
  : F[Either[LogicalError, PlayerData]] = for {
    extracted   <- extractFor(playerId, direction)
    transformed  = extracted.flatMap(
      Seq(gameNotSucceeded, gameNotFailed, damageFromHunger, starvedButAlive,
        tryOneStep, hitButAlive, damageFromPoison, poisonedButAlive, fixThisStep) .act(_) )
    persisted   <- persistState(transformed)
  } yield persisted.map( _.player )


  private def extractFor[F[_] : Monad](playerId: PlayerId,
                            direction: AxisDirection)
                           (implicit players: Players.Accessor[F],
                            mazes: Mazes.Accessor[F],
                            tracings: Tracings.Accessor[F],
                            timelines: Timelines.Accessor[F],
                            mazechanges: MazesChanges.Accessor[F],
                            messages: MessageStrings.Service)
  : F[Either[LogicalError, GameState]] = (for {
    player  <- loadPlayerData(playerId)
    maze    <- loadMazeData(player)
    tracing <- loadTracingData(playerId)
    timeline<- loadTimelineData(playerId)
    changes <- loadChangesData(playerId)
    vector  <- toVector(maze, direction, player.coords,
        AxisDirection.applyTo(direction)(player.coords))
  } yield GameState(playerId, player, maze, tracing, timeline, changes, vector)).value

  private def persistState[F[_] : Monad]( stateE: Either[LogicalError, GameState])
                             (implicit  players: Players.Accessor[F],
                              tracings: Tracings.Accessor[F],
                              timelines: Timelines.Accessor[F],
                              mazechanges: MazesChanges.Accessor[F])
  : F[Either[LogicalError, GameState]] = {
    val stateOpt = stateE match {
      case Left(error) => error.gameStateOpt
      case Right(state) => Some(state)
    }
    val stateF: F[Either[LogicalError, GameState]] = stateOpt match {
      case None => implicitly[Monad[F]].pure(stateE)
      case Some(state) => for {
        _ <- players.createOrUpdate(state.id, state.player)
        _ <- tracings.createOrUpdate(TracingId(state.id), state.tracing)
        _ <- timelines.createOrUpdate(TimelineId(state.id), state.timeline)
        _ <- mazechanges.createOrUpdate(MazeChangesId(state.id), state.mazechanges)
      } yield stateE
    }
    stateF
  }


  private def loadMazeData[F[_] : Monad]
        (player: PlayerData)
        (implicit mazes: Mazes.Accessor[F], messages: MessageStrings.Service)
  : EitherT[F, LogicalError, MazeData] =
    EitherT(mazes.getOne(player.maze).map(_.toRight(mazeNotFound(player.maze.mazeNo))))
  private def loadPlayerData[F[_] : Monad]
        (playerId: PlayerId)
        (implicit players: Players.Accessor[F], messages: MessageStrings.Service)
  : EitherT[F, LogicalError, PlayerData] =
    EitherT(players.getOne(playerId).map(_.toRight(playerNotFound(playerId.login))))
  private def loadTracingData[F[_] : Monad]
        (playerId: PlayerId)
        (implicit tracings: Tracings.Accessor[F], messages: MessageStrings.Service)
  : EitherT[F, LogicalError, TracingData] =
    EitherT(tracings.getOne(TracingId(playerId)).map(_.toRight(playerNotFound(playerId.login))))
  private def loadTimelineData[F[_] : Monad]
        (playerId: PlayerId)
        (implicit timelines: Timelines.Accessor[F], messages: MessageStrings.Service)
  : EitherT[F, LogicalError, TimelineData] =
    EitherT(timelines.getOne(TimelineId(playerId)).map(_.toRight(playerNotFound(playerId.login))))
  private def loadChangesData[F[_] : Monad]
        (playerId: PlayerId)
        (implicit mazechanges: MazesChanges.Accessor[F], messages: MessageStrings.Service)
  : EitherT[F, LogicalError, MazeChangesData] =
    EitherT(mazechanges.getOne(MazeChangesId(playerId)).map(_.toRight(playerNotFound(playerId.login))))
  private def toVector[F[_] : Monad]
        (maze: MazeData, direction: AxisDirection, srcCoords: Coords, dstCoords: Coords)
        (implicit messages: MessageStrings.Service)
  : EitherT[F, LogicalError, ApplyVector] =
    EitherT(implicitly[Monad[F]].pure( (for {
      srcCell <- maze.select(srcCoords)
      dstCell <- maze.select(dstCoords)
    } yield ApplyVector(MazePosition(srcCoords, srcCell), direction, MazePosition(dstCoords, dstCell))
      ).toRight(outOfRange)))

}

object MazeLogic {
  def getAllIds[F[_]: Monad]()
                            (implicit mazes: Mazes.Accessor[F],
                             descriptions: MazeDescriptions.Accessor[F]): F[Seq[(MazeId, Option[MazeDescriptionData])]] =
    mazes.getAll.flatMap { seq =>
      descriptions.getAll.map( desc => seq.map(maze => (maze.id, desc.get(MazeDescriptionId(maze.id)))))
    }

  def addMazeLayout[F[_]: Monad](radius: Int, layout: String)
                                (implicit mazes: Mazes.Accessor[F], messages: MessageStrings.Service): F[Either[LogicalError, MazeId]] = {
    val layoutOpt = MazeParseOps.parseLayout(layout, radius)
    layoutOpt match {
      case None => implicitly[Monad[F]].pure(Left(mazeLayoutBroken()))
      case Some(data) => mazes.addOne(data).map(Right(_))
    }
  }

  def updMazeLayout[F[_]: Monad](maze: MazeId, radius:Int, layout: String)
                                (implicit mazes: Mazes.Accessor[F], messages: MessageStrings.Service): F[Either[LogicalError, MazeId]] = {
    val layoutOpt = MazeParseOps.parseLayout(layout, radius)
    layoutOpt match {
      case None => implicitly[Monad[F]].pure(Left(mazeLayoutBroken()))
      case Some(data) => mazes.updOne(maze, data).map(Right(_))
    }
  }

}
