package zurtax.escape.game

import cats.syntax.eq._
import zurtax.escape.config.MessageStrings


object GameStateActions {
  import GameState._
  import LogicalError._

  def gameNotSucceeded(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case x: GameState if x.vector.src.cell.eq(Exit) => exitPosition()
    }
  }

  def gameNotFailed(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case x: GameState if gameHealth.get(x) < 1 =>
        gameFailedAlready(Some(gameTimeline.modify(_.gameover(x.player))(x)))
    }
  }

  private def damage(damage: DifficultySettings=>Int)(state: GameState)(implicit dices: DiceSet.Service): GameState =
    gameHealth.modify(_ - dices.rollDices(DiceSet(1, damage(gameDiffic.get(state)))).sum())(state)

  def damageFromPoison(implicit messages: MessageStrings.Service, dices: DiceSet.Service)
  : LogicTransformer = new LogicTransformer {
    override val transform = {
      case state: GameState if
        (state.vector.src.cell === Poison && state.player.coords == state.vector.src.coords) ||
        (state.vector.dst.cell === Poison && state.player.coords == state.vector.dst.coords) =>
        damage( _.poisonDamage.abs )(
          gameTimeline.modify( _.poisoned(state.player) )(
            state))
    }
  }

  def poisonedButAlive(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case state: GameState if gameHealth.get(state) < 1 =>
        gameFailedPoison(gameTimeline.modify(_.poisonedToDeath(state.player))(
          state))
    }
  }

  def damageFromHunger(implicit messages: MessageStrings.Service, dices: DiceSet.Service)
  : LogicTransformer = new LogicTransformer {
    override val transform = {
      case state: GameState if (gameDiffic.get(state).hungerPeriod != 0
        && gameSteps.get(state) != 0
        && (gameSteps.get(state) % gameDiffic.get(state).hungerPeriod == 0)) =>
        damage(_.hungerDamage.abs)(
          gameTimeline.modify(_.starve(state.player))(
            state))
    }
  }

  def starvedButAlive(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case state: GameState if gameHealth.get(state) < 1 =>
        gameFailedHunger(gameTimeline.modify(_.starvedToDeath(state.player))(
          state))
    }
  }

  def nutsArePresent(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case state: GameState if gameShots.get(state) < 1 =>
        nutsAreOver(gameTimeline.modify(
          _.check(
            playerData = state.player,
            direction = state.vector.direction,
            failure = Some(messages.nutsAreOver())))(
          state))
    }
  }

  def checkFlatThrowing(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case state: GameState if
        (state.vector.direction === Up   && !(state.vector.src.cell === StairsUp)) ||
        (state.vector.direction === Down && !(state.vector.src.cell === StairsDown)) =>
        ceilingNutThrow(gameTimeline.modify(
          _.check(
            playerData = state.player,
            direction = state.vector.direction,
            failure = Some(messages.ceilingNutThrow())))(
          state))
    }
  }

  def checkFlatMoving(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case state: GameState if
      (state.vector.direction === Up   && !(state.vector.src.cell === StairsUp)) ||
        (state.vector.direction === Down && !(state.vector.src.cell === StairsDown)) =>
        ceilingPlayerMove(gameTimeline.modify(
          _.visit(
            playerData = state.player,
            direction = state.vector.direction,
            failure = Some(messages.ceilingPlayerMove())))(
          state))
    }
  }

  def movementWithoutFence(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case state: GameState if state.vector.dst.cell === Fence =>
        fencePosition(
          // разведать ячейку
          gameTimeline.modify(_.visit(state.player, state.vector.direction, Some(messages.fencePosition())))(
            // получить болты
            gameShots.modify(_ + gameMazeChanges.get(state).getOrElse(state.vector.dst.coords))(
              gameMazeChanges.modify(_.removeShots(state.vector.dst.coords)._1)(
                state)))
        )
    }
  }

  def movementWithoutWall(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case state: GameState if state.vector.dst.cell === Wall =>
        wallPosition(
          // разведать ячейку
          gameTimeline.modify(_.visit(state.player, state.vector.direction, Some(messages.wallPosition())))(
            // получить болты
            gameShots.modify(_ + gameMazeChanges.get(state).getOrElse(state.vector.dst.coords))(
              gameMazeChanges.modify(_.removeShots(state.vector.dst.coords)._1)(
                state)))
        )
    }
  }

  def movementWithoutCrate(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case state: GameState if state.vector.dst.cell === Crate => Obstacle(state)
    }
  }

  def hitButAlive(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case state: GameState if gameHealth.get(state) < 1 =>
        gameFailedImpact(gameTimeline.modify(_.impactToDeath(state.player))(state))
    }
  }

  def exitNotReached(implicit messages: MessageStrings.Service)
  : LogicVerifier = new LogicVerifier {
    override val verify = {
      case state: GameState if state.vector.dst.cell.eq(Exit) => exitPosition(
        Some(gameTimeline.modify(_.exitPointReached(state.player))(state))
      )
    }
  }

  def destroy(implicit messages: MessageStrings.Service) : LogicTransformer = new LogicTransformer {
    override val transform = {
      case state: GameState =>
        // ломаем ящик
        gameMazeChanges.modify(_ :+ (state.vector.dst.coords, Tile))(state)
    } }

  def scratch(implicit messages: MessageStrings.Service, dices: DiceSet.Service): LogicTransformer = new LogicTransformer {
    override val transform = {
      case state: GameState =>
        // наносим раны
        damage(_.crateDamage.abs)(
          gameTimeline.modify(_.scratch(state.player))(
            state))
    } }

  def impact(implicit messages: MessageStrings.Service, dices: DiceSet.Service): LogicTransformer = new LogicTransformer {
    override val transform = {
      case state: GameState =>
        // наносим раны
        damage(_.wallDamage.abs)(
          gameTimeline.modify(_.impact(state.player))(
            state))
    } }

  val stub: EitherLogic = new LogicTransformer {
    override val transform = { case state: GameState => state }
  }

  def doThrowNut(implicit messages: MessageStrings.Service)
  : LogicTransformer = new LogicTransformer {
    override val transform = {
      case state: GameState =>
        // добавляем 1 ячейку в трейс
        gameTracing.modify(_.revealCell(state.maze, state.mazechanges, state.vector.dst.coords))(
          // добавляем кадрр в журнал
          gameTimeline.modify(_.check(state.player, state.vector.direction, None))(
            // уменьшаем на 1 число болтов и гаек
            gameShots.modify(_ - 1)(
              // добавляем болт в изменения
              gameMazeChanges.modify(_.updated(state.vector.dst.coords, 1))(
                state))))
    }
  }

  def changePlayerCell(implicit messages: MessageStrings.Service) : LogicTransformer = new LogicTransformer {
    override val transform = {
      case state: GameState =>
        val newstate =
        // подбираем болты
          gameShots.modify(_ + gameMazeChanges.get(state).getOrElse(state.vector.dst.coords))(
            gameMazeChanges.modify(_.removeShots(state.vector.dst.coords)._1)(
              // смещаемся
              gamePlCoords.modify(_ => state.vector.dst.coords)(
                gameTimeline.modify(_.visit(state.player, state.vector.direction))(
                  state))))
        // разведать ячейку
        gameTracing.modify(_.visitCell(newstate.maze, newstate.mazechanges, newstate.vector.dst.coords))(newstate)
    } }

  def fixThisStep: LogicTransformer = new LogicTransformer {
    override val transform = {
      case state: GameState =>
        // увеличиваем счетчик
        gameSteps.modify(_+1)(state)
    } }

  // все возможные перемещения:
  // 1. в выход   - если успех, тогда "шаг" (и возвращаем успех)
  def toExitOr(fork: EitherLogic)(implicit messages: MessageStrings.Service): EitherLogic =
    exitNotReached fork (changePlayerCell, fork)

  // 2. в потолок - если успех, тогда ничего (и возвращаем успех)
  def toCeilingOr(fork: EitherLogic)(implicit messages: MessageStrings.Service): EitherLogic =
    checkFlatMoving fork (stub, fork)

  // 3. в ограду - если успех, тогда ничего (и возвращаем успех)
  def toFenceOr(fork: EitherLogic)(implicit messages: MessageStrings.Service): EitherLogic =
    movementWithoutFence fork (stub, fork)

  // 4. в стену   - если успех, тогда "наносим раны" (и возвращаем успех)
  def toWallOr(fork: EitherLogic)(implicit messages: MessageStrings.Service, dices: DiceSet.Service): EitherLogic =
    movementWithoutWall fork(impact, fork)

  // 5. в ящик    - если успех, тогда "наносим раны", "меняем ячейку", "шаг" (и возвращаем успех)
  def toCrateOr(fork: EitherLogic)(implicit messages: MessageStrings.Service, dices: DiceSet.Service): EitherLogic =
    movementWithoutCrate fork (scratch and destroy and changePlayerCell, fork)

  // 6. все остальные - если успех, тогда "шаг", "подбираем болты", "разведываем ячейку" (и возвращаем успех)
  def tryOneStep(implicit messages: MessageStrings.Service, dices: DiceSet.Service): EitherLogic =
    toExitOr( toCeilingOr( toFenceOr( toWallOr( toCrateOr( changePlayerCell ) ) ) ) )

}
