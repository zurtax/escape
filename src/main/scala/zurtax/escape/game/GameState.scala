package zurtax.escape.game

import zurtax.escape.{Health, Shots, Steps}

final case class MazePosition(coords: Coords, cell: MazeCell)
final case class ApplyVector(src: MazePosition, direction: AxisDirection, dst: MazePosition)

final case class GameState (id: PlayerId,
                      player: PlayerData,
                      maze: MazeData,
                      tracing: TracingData,
                      timeline: TimelineData,
                      mazechanges: MazeChangesData,
                      vector: ApplyVector)

object GameState {
  import PlayerOps._
  import monocle.Lens
  import monocle.macros.GenLens

  val gamePlayer:       Lens[GameState, PlayerData] =
    GenLens[GameState](_.player)
  val gameTimeline:     Lens[GameState, TimelineData] =
    GenLens[GameState](_.timeline)
  val gameTracing:      Lens[GameState, TracingData] =
    GenLens[GameState](_.tracing)
  val gameMazeChanges:  Lens[GameState, MazeChangesData] =
    GenLens[GameState](_.mazechanges)
  val gamePlCoords:     Lens[GameState, Coords] =
    gamePlayer composeLens dataCoords
  val gameDiffic:     Lens[GameState, DifficultySettings] =
    gamePlayer composeLens dataDiffic
  val gameHealth:     Lens[GameState, Health] =
    gamePlayer composeLens dataHealth
  val gameShots:      Lens[GameState, Shots] =
    gamePlayer composeLens dataShots
  val gameSteps:      Lens[GameState, Steps] =
    gamePlayer composeLens dataSteps

  type EitherLogic = GameAction[Either, LogicalError, GameState]
  implicit val eitherErrorMapper: GameActionOps[Either, LogicalError, GameState] =
    new GameActionOps[Either, LogicalError, GameState] {

      override def and(first: EitherLogic, second: EitherLogic): EitherLogic =
        (state: GameState) => first.act(state).flatMap(second.act)

      override def or(first: EitherLogic, second: EitherLogic): EitherLogic =
        (state: GameState) => first.act(state) match {
          case Left(fail) => if (recover.isDefinedAt(fail)) second.act(recover(fail)) else second.act(state)
          case Right(succ) => Right(succ)
        }

      override def all(first: EitherLogic, second: EitherLogic): EitherLogic =
        (state: GameState) => first.act(state) match {
          case Left(fail) => if (recover.isDefinedAt(fail)) second.act(recover(fail)) else second.act(state)
          case Right(succ) => second.act(succ)
        }

      override def both(first: EitherLogic, second: EitherLogic): EitherLogic =
        (state: GameState) => first.act(state) match {
          case Left(fail) => if (recover.isDefinedAt(fail)) second.act(recover(fail)) else second.act(state)
          case Right(succ) => second.act(succ) match {
            case Left(_) => Right(succ)
            case Right(value) => Right(value)
          }
        }

      override def fork(first: EitherLogic, left : EitherLogic, right: EitherLogic): EitherLogic =
        (state: GameState) => first.act(state) match {
          case Left(fail) => if (recover.isDefinedAt(fail)) left.act(recover(fail)) else left.act(state)
          case Right(succ) => right.act(succ)
        }

      override val recover: PartialFunction[LogicalError, GameState] = {
        case e:LogicalError if e.gameStateOpt.isDefined => e.gameStateOpt.get
      }
    }

  abstract class LogicVerifier extends Verifier[Either, LogicalError, GameState] {
    override val success = Right(_)
    override val failure = Left(_)
  }

  abstract class LogicTransformer extends Transformer[Either, LogicalError, GameState] {
    override val success = Right(_)
  }
}
