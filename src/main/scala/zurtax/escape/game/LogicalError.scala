package zurtax.escape.game

import zurtax.escape.EscapeError
import zurtax.escape.config.MessageStrings

sealed abstract class LogicalError(message: String, val gameStateOpt: Option[GameState] = None)
  extends EscapeError(message)

object LogicalError {

  case class PlayerNotFound(message: String) extends LogicalError(message)

  case class MazeNotFound(message: String) extends LogicalError(message)

  case class MazeAlreadyExists(message: String) extends LogicalError(message)

  case class MazeLayoutBroken(message: String) extends LogicalError(message)

  case class WrongDirection(message: String) extends LogicalError(message)

  case class OutOfRange(message: String) extends LogicalError(message)

  case class ExitPosition(message: String, stateOpt: Option[GameState] = None) extends LogicalError(message, stateOpt)

  case class HealthLimitOut(reason: String, stateOpt: Option[GameState] = None) extends LogicalError(reason, stateOpt)

  case class WallPosition(message: String, state: GameState) extends LogicalError(message, Some(state))

  case class FencePosition(message: String, state: GameState) extends LogicalError(message, Some(state))

  case class NutsAreOver(message: String, state: GameState) extends LogicalError(message, Some(state))

  case class CeilingNutThrow(message: String, state: GameState) extends LogicalError(message, Some(state))

  case class CeilingPlayerMove(message: String, state: GameState) extends LogicalError(message, Some(state))

  // внутренний служебный класс
  private[game] case class Obstacle(state: GameState) extends LogicalError("Obstacle", Some(state))

  def playerNotFound(name: String)(implicit config: MessageStrings.Service): PlayerNotFound =
    PlayerNotFound(config.playerNotFound(name))

  def mazeNotFound(number: Int)(implicit config: MessageStrings.Service): MazeNotFound =
    MazeNotFound(config.mazeNotFound(number))

  def mazeAlreadyExists(number: Int)(implicit config: MessageStrings.Service): MazeAlreadyExists =
    MazeAlreadyExists(config.mazeAlreadyExists(number))

  def mazeLayoutBroken()(implicit config: MessageStrings.Service): MazeLayoutBroken =
    MazeLayoutBroken(config.mazeLayoutBroken())

  def wrongDirection(direction: String)(implicit config: MessageStrings.Service): WrongDirection =
    WrongDirection(config.wrongDirection(direction))

  def outOfRange()(implicit config: MessageStrings.Service): OutOfRange =
    OutOfRange(config.outOfRange())

  def exitPosition(stateOpt: Option[GameState] = None)(implicit config: MessageStrings.Service): ExitPosition =
    ExitPosition(config.exitPosition(), stateOpt)

  def gameFailedAlready(stateOpt: Option[GameState] = None)(implicit config: MessageStrings.Service): HealthLimitOut =
    HealthLimitOut(config.gameFailedMsg(), stateOpt)

  def gameFailedPoison(state: GameState)(implicit config: MessageStrings.Service): HealthLimitOut =
    HealthLimitOut(config.gamePoisonedMsg(), Some(state))

  def gameFailedHunger(state: GameState)(implicit config: MessageStrings.Service): HealthLimitOut =
    HealthLimitOut(config.gameStarvedMsg(), Some(state))

  def gameFailedImpact(state: GameState)(implicit config: MessageStrings.Service): HealthLimitOut =
    HealthLimitOut(config.gameImpactDeath(), Some(state))

  def wallPosition(state: GameState)(implicit config: MessageStrings.Service): WallPosition =
    WallPosition(config.wallPosition(), state)

  def fencePosition(state: GameState)(implicit config: MessageStrings.Service): FencePosition =
    FencePosition(config.fencePosition(), state)

  def nutsAreOver(state: GameState)(implicit config: MessageStrings.Service): NutsAreOver =
    NutsAreOver(config.nutsAreOver(), state)

  def ceilingNutThrow(state: GameState)(implicit config: MessageStrings.Service): CeilingNutThrow =
    CeilingNutThrow(config.ceilingNutThrow(), state)

  def ceilingPlayerMove(state: GameState)(implicit config: MessageStrings.Service): CeilingPlayerMove =
    CeilingPlayerMove(config.ceilingPlayerMove(), state)

}
