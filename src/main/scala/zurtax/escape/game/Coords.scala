package zurtax.escape.game

import derevo.circe.{codec, decoder, encoder}
import derevo.derive
import io.circe.{KeyDecoder, KeyEncoder}
import monocle.Lens
import monocle.macros.GenLens
import ru.tinkoff.tschema.swagger.Swagger
import doobie._

@derive(codec, Swagger, encoder, decoder)
sealed case class Coords(x: Int = 0, y: Int = 0, z: Int = 0)

object Coords {

  implicit def coordsToNavigation(coords: Coords): CoordsNavigation = CoordsNavigation(coords)

  case class CoordsNavigation(coords: Coords) {
    lazy val east: Coords  = coordX.modify(_ - 1)(coords)
    lazy val west: Coords  = coordX.modify(_ + 1)(coords)
    lazy val north: Coords = coordY.modify(_ - 1)(coords)
    lazy val south: Coords = coordY.modify(_ + 1)(coords)
    lazy val neighbors: Seq[Coords]     = List(north, west, south, east)
  }

  private val regex = raw"\{(\-?\d+),(\-?\d+),(\-?\d+)\}".r
  implicit val keyDecoder: KeyDecoder[Coords] = KeyDecoder.instance {
    case regex(x, y, z) => Some(Coords(x.toInt, y.toInt, z.toInt))
    case _              => None
  }
  implicit val keyEncoder: KeyEncoder[Coords] = KeyEncoder.instance { c: Coords => s"{${c.x},${c.y},${c.z}}" }

  implicit val coordsDBMeta: Meta[Coords] = Meta[String].timap(keyDecoder.apply(_).get)(keyEncoder.apply)

  val coordX: Lens[Coords, Int] = GenLens[Coords](_.x)
  val coordY: Lens[Coords, Int] = GenLens[Coords](_.y)
  val coordZ: Lens[Coords, Int] = GenLens[Coords](_.z)

}
