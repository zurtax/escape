package zurtax.escape.game


object MazeParseOps {

  /* check 11.1803(sqrt(5^3)) == 11 or 5(cqrt(5^3)) == 5*/
  val validate: Double => Option[Int] = d => if( d.toInt.toDouble==d) Some(d.toInt) else None


  // 9*9*9 = 729 = 27*27
  // по этому сначала ищем куб, потом квадрат
  def parseString(string: String): Option[MazeData] = {
    val layout = string.toCharArray.filterNot(_==0x7C).mkString
    val diameter = validate(Math.cbrt(layout.length)).getOrElse(validate(Math.sqrt(layout.length)).getOrElse(0))
    if (diameter>0) Some(readLayout(layout, diameter)).flatten else None
  }

  def parseLayout(string: String, radius: Int): Option[MazeData] = {
    val diameter = radius*2+1
    val sqare = diameter*diameter
    val cubic = sqare*diameter
    if (string.length==sqare || string.length==cubic) Some(readLayout(string, diameter)).flatten else None
  }

  def readLayout(string: String, diameter: Int): Option[MazeData] = {
    val matrix = string.
      toCharArray.
      map(MazeCell(_)).
      grouped(diameter). // one floor
      grouped(diameter). // pack of floors
      filter( filterFloor ).
      map( matter ).
      toVector
    if ( matrix.size==1 || matrix.size==diameter )
      Some(MazeData(matrix))
    else
      None
  }

  def writeLayout(mazeData: MazeData): String =
    mazeData.layout.foldLeft(List[String]())( (acc, f) => acc :+ writeFloor(f)).mkString("|")

  def writeFloor(floor: MazeFloor): String =
    floor.floor.foldLeft("")((acc, r) => r.row.foldLeft(acc)((acc2, c) => acc2 + c.print))

  val valid = Vector(Fence, Exit)
  val checkOuter: Array[MazeCell] => Boolean =
    seq => seq.forall(valid.contains(_)) && seq.head==Fence && seq.last==Fence
  val checkInner: Array[MazeCell] => Boolean =
    seq => valid.contains(seq.head) && valid.contains(seq.last)
  val filterFloor: Seq[Array[MazeCell]] => Boolean =
    floor => checkOuter(floor.head) && checkOuter(floor.last) && floor.tail.dropRight(1).forall(checkInner)
  val matter: Seq[Array[MazeCell]] => MazeFloor =
    floor => MazeFloor(floor.map(row => MazeRow(row.toVector)).toVector)

}
