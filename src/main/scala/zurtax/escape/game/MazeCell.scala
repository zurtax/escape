package zurtax.escape.game

import derevo.circe.codec
import derevo.derive
import ru.tinkoff.tschema.swagger.Swagger
import scala.util.Random
import doobie.Meta

sealed trait MazeCellProperty
// разрушаемость (возможность превратить ячейку в другой тип)
// пока что имеет смысл только для непроходимых, но это потому что типов ячеек мало,
// а так это независимые параметры, поэтому от базового трейта
trait Destructible   extends MazeCellProperty
trait Indestructible extends MazeCellProperty
// проходимость
trait Passable   extends MazeCellProperty
trait Impassable extends MazeCellProperty
// безопасность
trait Trapped extends MazeCellProperty
trait Safe    extends MazeCellProperty

@derive(codec, Swagger)
sealed trait MazeCell
@derive(codec, Swagger)
object Tile extends MazeCell with Passable with Safe // walkable safe tile
@derive(codec, Swagger)
object Poison extends MazeCell with Passable with Trapped with Indestructible // walkable poisoned tile
@derive(codec, Swagger)
object Wall extends MazeCell with Impassable with Indestructible // inner undestructile immobile block
@derive(codec, Swagger)
object Crate extends MazeCell with Impassable with Destructible // inner destructible/moveable block
@derive(codec, Swagger)
object Fence extends MazeCell with Impassable with Indestructible // outer wall
@derive(codec, Swagger)
object Exit extends MazeCell with Passable with Safe // exit point in the perimeter fence

@derive(codec, Swagger)
case class Stairs(direction: AxisDirection) extends MazeCell with Passable with Safe // staircase for 3d
@derive(codec, Swagger)
object StairsUp extends Stairs(Up)
@derive(codec, Swagger)
object StairsDown extends Stairs(Down)

object MazeCell {
  import cats.Show
  import cats.syntax.show._
  import cats.Eq
  import cats.syntax.eq._
  import cats.instances.string._

  val types = Vector(Poison, Wall, Crate, Tile)


  /** Получает ячейку из набора по индексу, если возможно. Если индекс меньше нуля, первый элемент,
    * если больше размера набора, последний.
    * @param num индекс
    * @return ячейка
    * @see [[MazeCell#types]]
    */
  def apply(num: Int): MazeCell with MazeCellProperty =
    if (num < 0) types.head
    else if (num >= types.size) types.last
    else types(num)

  def rand: MazeCell with MazeCellProperty =
    apply(Random.nextInt(types.size + 2))

  def apply(char: Char): MazeCell = char match {
    case '▒' => Poison
    case '█' => Wall
    case '■' => Crate
    case '√' => Exit
    case '^' => StairsUp
    case 'v' => StairsDown
    case '╝' => Fence
    case '╚' => Fence
    case '╗' => Fence
    case '╔' => Fence
    case '═' => Fence
    case '║' => Fence
    case '╬' => Fence
    case _   => Tile
  }

  implicit class MazeCellPrinter(cell: MazeCell) {
    val print: Char = cell match {
      case Tile => ' '
      case Poison => '▒'
      case Wall => '█'
      case Crate => '■'
      case Fence => '╬'
      case Exit => '√'
      case Stairs(direction) if direction === Up => '^'
      case Stairs(_) => 'v'
    }
  }

  implicit val mazeCellShow: Show[MazeCell] = {
    case Tile   => "Tile"
    case Poison => "Poison"
    case Wall   => "Wall"
    case Crate  => "Crate"
    case Fence  => "Fence"
    case Exit   => "Exit"
    case Stairs(dir) =>
      s"S[${dir.show}]" // у нас зависимый show для MazeRow на табах, поэтому укладываем в длину сокращением названия
  }

  implicit val mazeCellDBMeta: Meta[MazeCell] =
    Meta[String].timap(s => MazeCell.apply(s.charAt(0)))(MazeCellPrinter(_).print.toString)

  // не уверен, что нужен, ведь те, что объекты и так с правильным сравнением,
  // а для case классов (лестница) equals генерится, но пусть остается пока
  implicit val mazeCellEqual: Eq[MazeCell] =
    Eq.instance[MazeCell] { (cell1, cell2) => cell1.show === cell2.show }
}
