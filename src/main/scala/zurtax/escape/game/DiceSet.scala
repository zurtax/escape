package zurtax.escape.game

final case class DiceRoll (roll: Seq[Int]) {
  private def ignoreOne(f: (Int,Int)=>Boolean): DiceRoll = roll match {
    case head::tail =>
      DiceRoll(tail.foldLeft((head, Seq[Int]())) {
        (acc, next) => if (f(acc._1,next)) (next, acc._2 appended acc._1) else (acc._1, acc._2 appended next)
      }._2)
    case x => DiceRoll(Nil)
  }
  def ignoreLowest() : DiceRoll = ignoreOne( _ > _)
  def ignoreHiest()  : DiceRoll = ignoreOne( _ < _)
  def sum(): Int = if(roll.nonEmpty) roll.sum else 0
  def min(): Int = if(roll.nonEmpty) roll.min else 0
  def max(): Int = if(roll.nonEmpty) roll.max else 0
}

final case class DiceSet (dices: Int, sides: Int) {
  def roll(rand: Int=>Int) : DiceRoll = DiceRoll( Seq.fill(dices)(rand(sides)+1))
}

object DiceSet {
  def apply(pattern: String): DiceSet =  pattern.split('d') match {
    case Array()  => DiceSet(1, 6)
    case Array(x) => DiceSet(1, x.toIntOption.getOrElse(6))
    case ds => DiceSet( ds(0).toIntOption.getOrElse(1), ds(1).toIntOption.getOrElse(6) )
  }

  trait Service {
    def rollDices(set: DiceSet): DiceRoll
  }

}