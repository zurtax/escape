package zurtax.escape.game

import zio.{URLayer, ZIO, ZLayer}
import zurtax.escape.config.MessageStrings
import zurtax.escape.dao.{MazeDescriptions, Mazes, MazesChanges, Players, Timelines, Tracings}
import zurtax.escape.{Escape, LogicEnv, Player, StringsEnv}

import scala.util.Random


trait LogicService {
  // Player service
  def createNewGame(playerId: PlayerId, mazeId: MazeId, difficulty: DifficultySettings): Escape[Player]
  def getPlayerStatus(playerId: PlayerId): Escape[PlayerData]
  def getPlayerAutomap(playerId: PlayerId): Escape[String]
  def getPlayerTimeline(playerId: PlayerId): Escape[Seq[String]]
  def checkMazeCell(playerId: PlayerId, direction: AxisDirection): Escape[MazeCell]
  def makeOneStep(playerId: PlayerId, direction: AxisDirection): Escape[PlayerData]
  // Maze service
  def mazeAddString(radius: Int, layout: String): Escape[MazeId]
  def mazeUpdString(maze: MazeId, radius:Int, layout: String): Escape[MazeId]
  def all(): Escape[Seq[(Int, String)]]
}

object LogicService {
  val random = new DiceSet.Service{
    val rand = new Random()
    override def rollDices(set: DiceSet): DiceRoll = set.roll(rand.nextInt)
  }
  val live: URLayer[StringsEnv, LogicEnv] =
    ZLayer.fromEffect(ZIO.access[StringsEnv](config => new GameMechanics(config.get, random)))
}

class GameMechanics (val strings: MessageStrings.Service, val dices: DiceSet.Service) extends LogicService {

  // импортируем имплиситы
  import Mazes._
  import MazesChanges._
  import MazeDescriptions._
  import Players._
  import Timelines._
  import Tracings._
  implicit val config: MessageStrings.Service = strings
  implicit val barrel: DiceSet.Service = dices
  implicit def absolver[A](effect: Escape[Either[LogicalError, A]]): Escape[A] = ZIO.absolve(effect)


  /**
   * Добавить новый лабиринт, распарсив строку
   *
   * @param radius радиус добавляемого лабиринта
   * @param layout строка для разбора
   * @return готовый лабиринт, если не удалось распарсить, то получим ошибку
   */
  def mazeAddString(radius: Int, layout: String): Escape[MazeId] =
    MazeLogic.addMazeLayout[Escape](radius, layout)


  /**
   * Изменить существующий лабиринт, распарсив строку
   *
   * @param mazeId который лабиринт надо заменить
   * @param radius радиус нового лабиринта
   * @param layout строка для разбора
   * @return готовый лабиринт, если не удалось распарсить, то получим ошибку
   */
  def mazeUpdString(mazeId: MazeId, radius: Int, layout: String): Escape[MazeId] =
    MazeLogic.updMazeLayout[Escape](mazeId, radius, layout)

  /**
   * Списк всех номеров лабиринтов
   *
   * @return номера лабиринтов
   */
  def all(): Escape[Seq[(Int, String)]] =
    MazeLogic.getAllIds[Escape]().map(_.map(d => (d._1.mazeNo, d._2.map(_.description).getOrElse(""))))

  /**
   * Создать новую игру. Старая потеряется. В новой игре
   *
   * @param playerId Игрок начинает искать выход из центра
   * @param mazeId   Лабиринта
   * @return Новое состояние игры. Накапливать ошибки смысла нет.
   *         Она одна: Не найден лабиринт. Если учесь планы по
   *         генерированию лабиринта, то и она может испариться.
   */
  def createNewGame(playerId: PlayerId, mazeId: MazeId,
                    difficulty: DifficultySettings = DifficultySettings()): Escape[Player] =
    ZIO.absolve(PlayerLogic.createNewGame[Escape](playerId, mazeId, difficulty))
      .flatMap(pd => Entities.makeEntity(playerId, pd))


  /**
   * Получить статус игры, состояние игрока. Load saved game.
   *
   * @param playerId Игрок
   * @return Состояние игры для игрока
   */
  def getPlayerStatus(playerId: PlayerId): Escape[PlayerData] =
    PlayerLogic.getPlayerStatus[Escape](playerId)


  /**
   * Получить схаму разведанной области.
   *
   * @param playerId Кто хочет посмотреть карту.
   * @return ASCII строка с картой местности.
   */
  def getPlayerAutomap(playerId: PlayerId): Escape[String] =
    PlayerLogic.getPlayerAutomap[Escape](playerId)


  /**
   * Журнал игрока, события игры в хронологическом порядке
   *
   * @param playerId Чей журнал
   * @return текстовая интерпиритация событий
   */
  def getPlayerTimeline(playerId: PlayerId): Escape[Seq[String]] =
    PlayerLogic.getPlayerTimeline[Escape](playerId)


  /**
   * Уточняем тип соседней ячейки, уменьшаем число болтов.
   *
   * @param playerId  игрок/игра
   * @param direction направление метания болта или гайки
   * @return точный тип клетки и новое состояние игрока
   */
  def checkMazeCell(playerId: PlayerId, direction: AxisDirection): Escape[MazeCell] =
    PlayerLogic.checkMazeCell[Escape](playerId, direction)

  /**
   * Попытка сделать шаг в указанном направлении.
   *
   * @param playerId  кто шагает
   * @param direction куда шагает
   * @return новое состояние игры или игрока
   */
  def makeOneStep(playerId: PlayerId, direction: AxisDirection): Escape[PlayerData] =
    PlayerLogic.makeOneStep[Escape](playerId, direction)

}
