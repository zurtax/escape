package zurtax.escape.dao

import java.time.OffsetDateTime

import zio.{Ref, UIO, URLayer, ZLayer}
import zurtax.escape.{ConfigEnv, Escape, SystemEnv, MazeChanges, MazesChangesEnv}
import zurtax.escape.game.{ActionInfo, Entity, PlayerId, MazeChangesData, MazeChangesId}

/**
 * Класс для работы с изменениями лабиринтов игроков
 * @param ref - стартовый набор
 */
class MazesChangesRefService(ref: Ref[MazesChangesRefService.State]) extends MazesChanges.Service {

  override def createOrUpdate(mazeChanges: MazeChanges): Escape[MazeChanges] = for {
    entity <- ref.modify(state => (mazeChanges, state + (mazeChanges.id.playerId.login -> mazeChanges)))
  } yield entity

  /**
   * Ищем изменения лабиринта и возвращаем если есть. Если их нет - это серьезная ошибка
   * целостности данных, но принимать решение надо уровнем выше.
   * @param mazeChangesId игра
   * @return параметры игры, если такая есть
   */
  override def getOne(mazeChangesId: MazeChangesId): Escape[Option[MazeChanges]] =
    ref.get.map(_.get(mazeChangesId.playerId.login))

}


object MazesChangesRefService {
  type State = Map[String, MazeChanges]

  /**
   * Стартовый набор из двух игроков
   */
  val initialState: State = Map(
    "test" -> Entity(
      id = MazeChangesId(PlayerId("test")),
      data = MazeChangesData(),
      created = ActionInfo(OffsetDateTime.now(), None)
    ),
    "keks" -> Entity(
      id = MazeChangesId(PlayerId("keks")),
      data = MazeChangesData(),
      created = ActionInfo(OffsetDateTime.now(), None)
    )
  )

  /**
   * Ф-ция для добавления в контекст. Не производит исключений,
   * для запуска не требует другого контекста, производит MazesChangesEnv.
   */

  val live: URLayer[SystemEnv with ConfigEnv, MazesChangesEnv] = ZLayer.fromEffect(
    for { state <- UIO.succeed(initialState); ref <- Ref.make(state) }
      yield new MazesChangesRefService(ref)
  )

}



