package zurtax.escape
package dao

import doobie.implicits._
import doobie.util.query.Query0
import doobie.util.transactor.Transactor
import doobie.util.update.Update0
import zio._
import zurtax.escape.dao.PlayerDbService.SQL
import zurtax.escape.game.{Entities, PlayerData, PlayerId}

/**
 * Реализация работы с нижним слоем данных по игрокам через Doobie
 * @param xa - та часть, через которую отправляются запосы к базе
 */
class PlayerDbService (xa: Transactor[Task]) extends Players.Service {

  override def createOrUpdate(player: Player): Escape[PlayerId] = for {
    _ <- SQL.remove(player.id).run.transact(xa)
    p = Entities.create(player.id, player.data)
    _ <- SQL.putOne(p).run.transact(xa)
  } yield player.id

  override def getOne(playerId: PlayerId): Escape[Option[Player]] =
    SQL.getOne(playerId).option.transact(xa)
}

object PlayerDbService {

  /**
   * Конструируем часть контекста.
   * На входе нужен контекст DB, для получения Transactor.
   * На выходе получаем PlayerEnv.
   */
  val live: URLayer[DB, PlayerEnv] =
    ZLayer.fromEffect(ZIO.access[DB](db => new PlayerDbService(db.get.transactor)))

  /**
   * Здесь наши запросы
   */
  object SQL {

    val toPlayer: ((PlayerId, PlayerData)) => Player = {
      case (id, data) => Entities.create(id, data)
    }

    val getAll: Query0[Player] =
      sql"SELECT p.login AS login, m.number AS mazeNo, p.hp, p.shots, p.diff, p.coords, p.steps FROM (players p JOIN mazes m ON p.maze_number=m.number)"
        .query[(PlayerId, PlayerData)] map toPlayer

    val getOne: PlayerId => Query0[Player] = p =>
      sql"SELECT p.login AS login, m.number AS mazeNo, p.hp, p.shots, p.diff, p.coords, p.steps FROM (players p JOIN mazes m ON p.maze_number=m.number AND p.login=${p.login})"
        .query[(PlayerId, PlayerData)] map toPlayer

    def putOne(player: Player): Update0 =
      sql"INSERT INTO players (login, maze_number, hp, shots, diff, coords, steps) VALUES (${player.id.login}, ${player.data.maze.mazeNo}, ${player.data.hp}, ${player.data.shots}, ${player.data.difficulty}, ${player.data.coords}, ${player.data.steps})"
        .update

    def remove(id: PlayerId): Update0 =
      sql"DELETE FROM players where login = ${id.login}".update
  }

}