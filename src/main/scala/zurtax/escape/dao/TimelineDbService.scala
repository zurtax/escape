package zurtax.escape
package dao

import doobie.free.connection.ConnectionIO
import doobie.implicits._
import doobie.util.transactor.Transactor
import doobie.util.fragment.Fragment
import doobie.util.update.{Update, Update0}
import scala.collection.immutable.ArraySeq
import zio._
import zurtax.escape.game.{Entities, PlayerId, TimeUnit, TimelineData, TimelineId}


/**
 * Реализация работы с логом игрока
 * @param xa - та часть, через которую отправляются запосы к базе
 */
class TimelineDbService (xa: Transactor[Task]) extends Timelines.Service {
  import TimelineDbService._

  override def createOrUpdate(timeline: Timeline): Escape[TimelineId] =
    SQL.putOne(timeline).transact(xa)

  override def getOne(timelineId: TimelineId): Escape[Option[Timeline]] =
    SQL.getOne(timelineId).transact(xa)
}

object TimelineDbService {
  import cats.implicits._
  import zurtax.escape.game.TimelineData._

  /**
   * Конструируем часть контекста.
   * На входе нужен контекст DB, для получения Transactor.
   * На выходе получаем TimelineEnv.
   */
  val live: URLayer[DB, TimelineEnv] =
    ZLayer.fromEffect(ZIO.access[DB](db => new TimelineDbService(db.get.transactor)))

  /**
   * Здесь наши запросы
   */
  object SQL {

    val getAll: ConnectionIO[Seq[Timeline]] =
      sql"SELECT player_login, step, hp, shots, act, events FROM timelines_units ORDER BY player_login"
        .query[(PlayerId, TimeUnit)].to[ArraySeq]
        .map( _.groupMap(_._1)(_._2) )
        .map( _.toList.map {
          case (p, s) => Entities.create(TimelineId(p), TimelineData(s.toVector)) })

    val getOne: TimelineId => ConnectionIO[Option[Timeline]] = id =>
      sql"SELECT step, hp, shots, act, events FROM timelines_units WHERE player_login=${timeIdLogin.get(id)}"
        .query[TimeUnit].to[ArraySeq]
        .map(_.toList match {
          case Nil => None
          case s => Some(Entities.create(id, TimelineData(s.toVector)))
        } )

    def putOne(timeline: Timeline): ConnectionIO[TimelineId] = for {
      _ <- remove(timeline.id).run
      sql = s"INSERT INTO timelines_units (player_login, step, hp, shots, act, events) VALUES ('${timelineLogin.get(timeline)}',?,?,?,?,?)"
      _ <- Update[TimeUnit](sql).updateMany(timeline.data.frames.toList)
    } yield timeline.id

    def remove(id: TimelineId): Update0 =
      sql"DELETE FROM timelines_units WHERE player_login=${timeIdLogin.get(id)}".update
  }

}