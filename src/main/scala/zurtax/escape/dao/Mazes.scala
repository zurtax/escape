package zurtax.escape.dao

import zio.RIO
import zurtax.escape.{Escape, Maze, MazeEnv}
import zurtax.escape.game.{MazeData, MazeId}

/**
 * Общий тип для работы с данными игр
 */
object Mazes  {

  trait Service {
    def getOne(mazeId: MazeId): Escape[Option[Maze]]
    def getAll: Escape[Seq[Maze]]
    def addOne(mazeData: MazeData): Escape[MazeId]
    def updOne(mazeId: MazeId, mazeData: MazeData): Escape[MazeId]
  }

  trait Accessor[F[_]] {
    def getOne(mazeId: MazeId): F[Option[MazeData]]
    def getAll: F[Seq[Maze]]
    def addOne(mazeData: MazeData): F[MazeId]
    def updOne(mazeId: MazeId, mazeData: MazeData): F[MazeId]
  }

  implicit val accessor: Accessor[Escape] = new Accessor[Escape] {
    private val toRIO = RIO.access[MazeEnv](_.get)

    override def getOne(mazeId: MazeId): Escape[Option[MazeData]] =
      toRIO .flatMap(_.getOne(mazeId)) .map(_.map(_.data))

    override def getAll: Escape[Seq[Maze]] =
      toRIO.flatMap(_.getAll)

    override def addOne(mazeData: MazeData): Escape[MazeId] =
      toRIO.flatMap(_.addOne(mazeData))

    override def updOne(mazeId: MazeId, mazeData: MazeData): Escape[MazeId] =
      toRIO.flatMap(_.updOne(mazeId, mazeData))
  }
}
