package zurtax.escape.dao

import java.time.OffsetDateTime

import zio.{Ref, UIO, URLayer, ZLayer}
import zurtax.escape.{ConfigEnv, Escape, Player, PlayerEnv, SystemEnv}
import zurtax.escape.game.{ActionInfo, DifficultySettings, Entity, MazeId, PlayerData, PlayerId}

/**
 * Класс для работы с набором игроков(игр) из Ref
 * @param ref - стартовый набор
 */
class PlayerRefService(ref: Ref[PlayerRefService.State]) extends Players.Service {

  override def createOrUpdate(player: Player): Escape[PlayerId] = for {
    entity <- ref.modify(state => (player, state + (player.id.login -> player)))
  } yield entity.id

  /**
   * Метод лезет в Ref и достает оттуда игрока/игру. Если нет - генерирует ошибку.
   * Сейачас это возвращаемая ошибка, но хотелось бы ошибку своего уровня
   * @param playerId игра
   * @return параметры игры, ессли такая есть
   */
  override def getOne(playerId: PlayerId): Escape[Option[Player]] =
    ref.get.map(_.get(playerId.login))

}

object PlayerRefService {
  type State = Map[String, Player]

  val difficulty: DifficultySettings = DifficultySettings()
  /**
   * Стартовый набор из двух игроков
   */
  val initialState:State = Map(
    "test" -> Entity(
      id = PlayerId("test"),
      data = PlayerData(
        maze  = MazeId(1),
        hp    = difficulty.startingHealth,
        shots = difficulty.startingShots,
        difficulty = difficulty),
      created = ActionInfo(OffsetDateTime.MIN, None)
    ),
    "keks" -> Entity(
      id = PlayerId("keks"),
      data = PlayerData(
        maze  = MazeId(3),
        hp    = difficulty.startingHealth,
        shots = difficulty.startingShots,
        difficulty = difficulty),
      created = ActionInfo(OffsetDateTime.MIN, None)
    )
  )

  /**
   * Ф-ция для добавления в контекст. Не производит исключений,
   * для запуска не требует другого контекста, производит PlayerEnv.
   */

  val live: URLayer[SystemEnv with ConfigEnv, PlayerEnv] = ZLayer.fromEffect(
    for { state <- UIO.succeed(initialState); ref <- Ref.make(state) }
      yield new PlayerRefService(ref)
  )

}
