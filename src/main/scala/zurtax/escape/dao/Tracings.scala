package zurtax.escape.dao

import zio.RIO
import zurtax.escape.game.{Entities, TracingData, TracingId}
import zurtax.escape.{Escape, Tracing, TracingEnv}

object Tracings {

  trait Service {
    def createOrUpdate(tracing: Tracing): Escape[TracingId]
    def getOne(tracingId: TracingId): Escape[Option[Tracing]]
  }

  trait Accessor[F[_]] {
    def createOrUpdate(tracingId:TracingId, tracingData: TracingData): F[TracingId]
    def getOne(tracingId: TracingId): F[Option[TracingData]]
  }

  implicit val tracingAccessor: Tracings.Accessor[Escape] = new Tracings.Accessor[Escape] {
    private val toRIO = RIO.access[TracingEnv](_.get)

    override def createOrUpdate(tracingId: TracingId, tracingData: TracingData): Escape[TracingId] =
      toRIO .flatMap( rio => Entities.makeEntity(tracingId, tracingData).flatMap(rio.createOrUpdate))

    override def getOne(tracingId: TracingId): Escape[Option[TracingData]] =
      toRIO .flatMap(_.getOne(tracingId)) .map(_.map(_.data))
  }

}
