package zurtax.escape

import zio.Has

package object dao {
  type DB = Has[DoobieContext.Service]
}
