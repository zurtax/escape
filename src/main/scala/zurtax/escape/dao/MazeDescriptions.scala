package zurtax.escape.dao

import zio.RIO
import zurtax.escape.game.{Entities, MazeDescriptionData, MazeDescriptionId}
import zurtax.escape.{Escape, MazeDescEnv, MazeDescription}

object MazeDescriptions {

  trait Service {
    def createOrUpdate(mazeDescription: MazeDescription): Escape[MazeDescriptionId]
    def getOne(mazeDescriptionId: MazeDescriptionId): Escape[Option[MazeDescription]]
    def getAll: Escape[Seq[MazeDescription]]
  }

  trait Accessor[F[_]] {
    def createOrUpdate(mazeDescriptionId: MazeDescriptionId, mazeDescriptionData: MazeDescriptionData): F[MazeDescriptionId]
    def getOne(mazeDescriptionId: MazeDescriptionId): F[Option[MazeDescriptionData]]
    def getAll: F[Map[MazeDescriptionId, MazeDescriptionData]]
  }

  implicit val mazeDescrAccessor: MazeDescriptions.Accessor[Escape] = new MazeDescriptions.Accessor[Escape] {
    private val toRIO = RIO.access[MazeDescEnv](_.get)

    override def createOrUpdate(mazeDescriptionId: MazeDescriptionId, mazeDescriptionData: MazeDescriptionData): Escape[MazeDescriptionId] =
      toRIO .flatMap( rio => Entities.makeEntity(mazeDescriptionId, mazeDescriptionData).flatMap(rio.createOrUpdate))

    override def getOne(mazeDescriptionId: MazeDescriptionId): Escape[Option[MazeDescriptionData]] =
      toRIO .flatMap(_.getOne(mazeDescriptionId)) .map(_.map(_.data))

    override def getAll(): Escape[Map[MazeDescriptionId, MazeDescriptionData]] =
      toRIO .flatMap(_.getAll.map(_.map(d => (d.id,d.data)).toMap))
  }

}
