package zurtax.escape
package dao

import doobie.implicits._
import doobie.util.query.Query0
import doobie.util.transactor.Transactor
import doobie.util.update.Update0
import zio._
import zurtax.escape.dao.MazeDbService.SQL
import zurtax.escape.game.{Entities, MazeData, MazeId}
import zurtax.escape.game.MazeParseOps._

/**
 * Реализация работы с нижним слоем данных по лабиринтам через Doobie
 * @param xa - та часть, через которую отправляются запосы к базе
 */
class MazeDbService (xa: Transactor[Task]) extends Mazes.Service {

  override def getOne(mazeId: MazeId): Escape[Option[Maze]] =
    SQL.getOne(mazeId).option.transact(xa).map(_.flatten)

  override def getAll: Escape[Seq[Maze]] = SQL.getAll.to[Vector].transact(xa).map(_.flatten)

  override def addOne(mazeData: MazeData): Escape[MazeId] = for {
    num <- SQL.getNewNumber.option.map(_.get).transact(xa)
    id = MazeId(num)
    maze = Entities.create(id, mazeData)
    _ <- SQL.putOne(maze).run.transact(xa)
  } yield id
  
  override def updOne(mazeId: MazeId, mazeData: MazeData): Escape[MazeId] = {
    SQL.updateOne(Entities.create(mazeId, mazeData)).run.transact(xa).map( _ => mazeId )
  }
}

object MazeDbService {

  /**
   * Конструируем часть контекста.
   * На входе нужен контекст DB, для получения Transactor.
   * На выходе получаем MazeEnv.
   */
  val live: URLayer[DB, MazeEnv] =
    ZLayer.fromEffect(ZIO.access[DB](db => new MazeDbService(db.get.transactor)))

  /**
   * Здесь наши запросы
   */
  object SQL {

    private val toMaze: ((MazeId, String)) => Option[Maze] = {
      case (mazeId, layout) => parseString(layout).map(Entities.create(mazeId, _))
    }

    val getNewNumber: Query0[Int] =
      sql"SELECT MAX(number) FROM mazes".query[Int].map(_ + 1)

    val getAll: Query0[Option[Maze]] =
      sql"SELECT number, layout FROM mazes".query[(MazeId, String)] map toMaze

    val getOne: MazeId => Query0[Option[Maze]] =
      id => sql"SELECT number AS mazeNo, layout FROM mazes WHERE number = $id".query[(MazeId, String)] map toMaze

    def putOne(maze: Maze): Update0 =
      sql"INSERT INTO mazes (number, layout) VALUES (${maze.id.mazeNo}, ${writeLayout(maze.data)})".update

    def updateOne(maze: Maze): Update0 =
      sql"UPDATE mazes SET layout = ${writeLayout(maze.data)} WHERE number = ${maze.id.mazeNo}".update

    def remove(id: MazeId): Update0 =
      sql"DELETE FROM mazes where number = ${id.mazeNo}".update
  }

}