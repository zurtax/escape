package zurtax.escape.dao


import java.time.OffsetDateTime

import zio.{Ref, UIO, URLayer, ZLayer}
import zurtax.escape.{ConfigEnv, Escape, SystemEnv, Tracing, TracingEnv}
import zurtax.escape.game.{ActionInfo, Entity, MazeData, PlayerId, TracingData, TracingId}

/**
 * Класс для работы с автокартами игроков
 * @param ref - стартовый набор
 */
class TracingRefService(ref: Ref[TracingRefService.State]) extends Tracings.Service {

  override def createOrUpdate(tracing: Tracing): Escape[TracingId] = for {
    entity <- ref.modify(state => (tracing, state + (tracing.id.playerId.login -> tracing)))
  } yield entity.id

  /**
   * Ищем карту и возвращаем ее если есть. Если ее нет - это серьезная ошибка
   * целостности данных, но принимать решение надо уровнем выше.
   * @param tracingId игра
   * @return параметры игры, если такая есть
   */
  override def getOne(tracingId: TracingId): Escape[Option[Tracing]] =
    ref.get.map(_.get(tracingId.playerId.login))

}

object TracingRefService {
  type State = Map[String, Tracing]

  /**
   * Стартовый набор из двух игроков
   */
  val initialState: State = Map(
    "test" -> Entity(
      id = TracingId(PlayerId("test")),
      data = TracingData(MazeData(1)),
        created = ActionInfo(OffsetDateTime.MIN, None)
      ),
      "keks" -> Entity(
        id = TracingId(PlayerId("keks")),
        data = TracingData(MazeData(3)),
        created = ActionInfo(OffsetDateTime.MIN, None)
        )
      )

  /**
   * Ф-ция для добавления в контекст. Не производит исключений,
   * для запуска не требует другого контекста, производит TracingEnv.
   */

  val live: URLayer[SystemEnv with ConfigEnv, TracingEnv] = ZLayer.fromEffect(
    for { state <- UIO.succeed(initialState); ref <- Ref.make(state) }
      yield new TracingRefService(ref)
  )

}
