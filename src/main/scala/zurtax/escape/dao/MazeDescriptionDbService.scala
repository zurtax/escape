package zurtax.escape
package dao

import doobie.implicits._
import doobie.util.query.Query0
import doobie.util.transactor.Transactor
import doobie.util.update.Update0
import zio.{Task, URLayer, ZIO, ZLayer}
import zurtax.escape.game.{Entities, MazeDescriptionData, MazeDescriptionId}


class MazeDescriptionDbService(xa: Transactor[Task]) extends MazeDescriptions.Service {
  import MazeDescriptionDbService.SQL

  override def createOrUpdate(mazeDescription: MazeDescription): Escape[MazeDescriptionId] = for {
    _ <- SQL.remOne(mazeDescription.id).run.transact(xa)
    _ <- SQL.addOne(mazeDescription.id, mazeDescription.data).run.transact(xa)
  } yield mazeDescription.id

  override def getOne(mazeDescriptionId: MazeDescriptionId): Escape[Option[MazeDescription]] =
    SQL.getOne(mazeDescriptionId).option.transact(xa)

  override def getAll: Escape[Seq[MazeDescription]] =
    SQL.getAll.stream.compile.to[Seq].transact(xa)
}

object MazeDescriptionDbService {

  val live: URLayer[DB, MazeDescEnv] =
    ZLayer.fromEffect(ZIO.access[DB](db => new MazeDescriptionDbService(db.get.transactor)))

  object SQL {
    val getAll: Query0[MazeDescription] =
      sql"SELECT maze_number, maze_description FROM maze_descriptions"
        .query[(MazeDescriptionId, MazeDescriptionData)]
        .map( p => Entities.create(p._1, p._2) )

    val getOne: MazeDescriptionId => Query0[MazeDescription] = id =>
      sql"SELECT maze_description FROM maze_descriptions WHERE maze_number=${id.mazeId.mazeNo}"
        .query[MazeDescriptionData]
        .map( data => Entities.create(id, data) )

    val remOne: MazeDescriptionId => Update0 = id =>
      sql"DELETE FROM maze_descriptions WHERE maze_number=${id.mazeId.mazeNo}"
        .update

    val addOne: (MazeDescriptionId, MazeDescriptionData) => Update0 = (id, data) =>
      sql"INSERT INTO maze_descriptions (maze_number, maze_description) VALUES (${id.mazeId.mazeNo}, ${data.description})"
        .update
  }
}