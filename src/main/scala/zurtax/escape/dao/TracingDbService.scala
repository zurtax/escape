package zurtax.escape
package dao

import doobie.free.connection.ConnectionIO
import doobie.implicits._
import doobie.util.transactor.Transactor
import doobie.util.update.{Update, Update0}
import zio._
import zurtax.escape.dao.TracingDbService.SQL
import zurtax.escape.game.{Entities, Floor, PlayerId, Revealed, TracingDataDB, TracingId}

import scala.collection.immutable.ArraySeq

/**
 * Реализация работы с автокартами игроков через Doobie
 * @param xa - та часть, через которую отправляются запосы к базе
 */
class TracingDbService (xa: Transactor[Task]) extends Tracings.Service {

  override def createOrUpdate(tracing: Tracing): Escape[TracingId] =
    SQL.putOne(tracing).transact(xa)

  override def getOne(tracingId: TracingId): Escape[Option[Tracing]] =
    SQL.getOne(tracingId).transact(xa)

  def getAll: Escape[Seq[Tracing]] = SQL.getAll.transact(xa)
}

object TracingDbService {

  /**
   * Конструируем часть контекста.
   * На входе нужен контекст DB, для получения Transactor.
   * На выходе получаем TracingEnv.
   */
  val live: URLayer[DB, TracingEnv] =
    ZLayer.fromEffect(ZIO.access[DB](db => new TracingDbService(db.get.transactor)))

  /**
   * Здесь наши запросы
   */
  object SQL {
    val getAll: ConnectionIO[Seq[Tracing]] =
      sql"SELECT player_login AS login, radius, plain, sized, tracing_id FROM tracings"
        .query[(PlayerId, TracingDataDB, Long)].to[ArraySeq]
        .flatMap { tracings =>
          sql"SELECT tracing_id, coords, cell FROM tracing_reveals"
            .query[(Long, Revealed)].to[ArraySeq]
            .map { allCells => allCells.groupMap(_._1)(_._2) }
            .map { grouped =>
              tracings
                .map { case (p, t, tid) => (TracingId(p),
                  t.toTD(grouped.getOrElse(tid, Vector()).groupMap(r => r.coords.z)(identity).toVector.sortBy(_._1)
                    .map { case (_, v) => Floor(v) }))
                }
                .map { case (tr, td) => Entities.create(tr, td) }
          }
        }

    import cats.implicits._
    val getOne: TracingId => ConnectionIO[Option[Tracing]] = id =>
      sql"SELECT radius, plain, sized, id FROM tracings WHERE player_login=${id.playerId.login}"
        .query[(TracingDataDB, Long)].option
        .flatMap { opt =>
          opt.traverse { case (tracingDB, tid) =>
            sql"SELECT coords, cell FROM tracing_reveals WHERE tracing_id=$tid".query[Revealed].to[ArraySeq]
              .map { allCells => allCells.groupMap(r => r.coords.z)(identity).toVector.sortBy(_._1).map { case (_, v) => Floor(v) } }
              .map { floors => Entities.create(id, tracingDB.toTD(floors)) }
          }
        }

    def putOne(tracing: Tracing): ConnectionIO[TracingId] = for {
      _ <- remove(tracing.id).run
      tid <- sql"INSERT INTO tracings(radius, plain, sized, player_login) VALUES (${tracing.data.radius}, ${tracing.data.plain}, ${tracing.data.sized}, ${tracing.id.playerId.login})"
        .update
        .withUniqueGeneratedKeys[Int]("id")
      sql = s"INSERT INTO tracing_reveals(coords, cell, tracing_id) VALUES (?,?,$tid)"
      _ <- Update[Revealed](sql).updateMany(tracing.data.floors.flatMap(_.revealed).toList)
    } yield tracing.id

    def remove(id: TracingId): Update0 =
      sql"DELETE FROM tracings WHERE player_login=${id.playerId.login}".update
  }

}