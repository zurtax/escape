package zurtax.escape.dao

import java.time.OffsetDateTime

import zio.{Ref, UIO, URLayer, ZLayer}
import zurtax.escape.{ConfigEnv, Escape, MazeDescEnv, MazeDescription, SystemEnv}
import zurtax.escape.game.{ActionInfo, Entity, MazeDescriptionData, MazeDescriptionId, MazeId}


class MazeDescriptionRefService(ref: Ref[MazeDescriptionRefService.State]) extends MazeDescriptions.Service {

  override def createOrUpdate(mazeDesc: MazeDescription): Escape[MazeDescriptionId] = for {
    entity <- ref.modify(state => (mazeDesc, state + (mazeDesc.id.mazeId.mazeNo -> mazeDesc)))
  } yield entity.id

  override def getOne(mazeDescId: MazeDescriptionId): Escape[Option[MazeDescription]] =
    ref.get.map(_.get(mazeDescId.mazeId.mazeNo))

  override def getAll: Escape[Seq[MazeDescription]] = ref.get.map(_.values.toSeq)
}

object MazeDescriptionRefService {
  type State = Map[Int, MazeDescription]

  val initialState:State = Map(
    1 -> Entity(
      id = MazeDescriptionId(MazeId(1)),
      data = MazeDescriptionData("Тренировочный лабиринт из одной ячейки."),
      created = ActionInfo(OffsetDateTime.MIN, None)),
    2 -> Entity(
      id = MazeDescriptionId(MazeId(2)),
      data = MazeDescriptionData("Тренировочный плоский лабиринт."),
      created = ActionInfo(OffsetDateTime.MIN, None)),
    3 -> Entity(
      id = MazeDescriptionId(MazeId(3)),
      data = MazeDescriptionData("Несложный плоский лабиринт."),
      created = ActionInfo(OffsetDateTime.MIN, None)),
    4 -> Entity(
      id = MazeDescriptionId(MazeId(4)),
      data = MazeDescriptionData("Плоский лабиринт средней сложности."),
      created = ActionInfo(OffsetDateTime.MIN, None)),
    5 -> Entity(
      id = MazeDescriptionId(MazeId(5)),
      data = MazeDescriptionData("Плоский лабиринт, немного сложнее."),
      created = ActionInfo(OffsetDateTime.MIN, None)),
    6 -> Entity(
      id = MazeDescriptionId(MazeId(6)),
      data = MazeDescriptionData("Плоский лабиринт, сложный."),
      created = ActionInfo(OffsetDateTime.MIN, None)),
    7 -> Entity(
      id = MazeDescriptionId(MazeId(7)),
      data = MazeDescriptionData("Плоский лабиринт, придется туго."),
      created = ActionInfo(OffsetDateTime.MIN, None)),
    8 -> Entity(
      id = MazeDescriptionId(MazeId(8)),
      data = MazeDescriptionData("Трехмерный несложный лабиринт."),
      created = ActionInfo(OffsetDateTime.MIN, None)),
    9 -> Entity(
      id = MazeDescriptionId(MazeId(9)),
      data = MazeDescriptionData("Трехмерный лабиринт для МЕГА-героев."),
      created = ActionInfo(OffsetDateTime.MIN, None))
    )

  val live: URLayer[SystemEnv with ConfigEnv, MazeDescEnv] = ZLayer.fromEffect(
    for { state <- UIO.succeed(initialState); ref <- Ref.make(state) }
      yield new MazeDescriptionRefService(ref)
  )

}