package zurtax.escape.dao

import zio.RIO
import zurtax.escape.game.{Entities, TimelineData, TimelineId}
import zurtax.escape.{Escape, Timeline, TimelineEnv}

object Timelines {

  trait Service {
    def createOrUpdate(timeline: Timeline): Escape[TimelineId]
    def getOne(timelineId: TimelineId): Escape[Option[Timeline]]
  }

  trait Accessor[F[_]] {
    def createOrUpdate(timelineId: TimelineId, timelineData: TimelineData): F[TimelineId]
    def getOne(timelineId: TimelineId): F[Option[TimelineData]]
  }

  implicit val timelineAccessor: Timelines.Accessor[Escape] = new Timelines.Accessor[Escape] {
    private val toRIO = RIO.access[TimelineEnv](_.get)

    override def createOrUpdate(playerId: TimelineId, playerData: TimelineData): Escape[TimelineId] =
      toRIO .flatMap( rio => Entities.makeEntity(playerId, playerData).flatMap(rio.createOrUpdate))

    override def getOne(changesId: TimelineId): Escape[Option[TimelineData]] =
      toRIO .flatMap(_.getOne(changesId)) .map(_.map(_.data))
  }

}
