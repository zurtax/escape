package zurtax.escape.dao

import zio.{RLayer, ZLayer}
import zurtax.escape.{ConfigEnv, EscapeError, ServiceEnv, SystemEnv}

/**
 * Объект-переключатель
 */
object ServiceContext {
  /**
   * Для работы с рефами
   */
  val ref: RLayer[SystemEnv with ConfigEnv, ServiceEnv] =
    MazeRefService.live ++ MazeDescriptionRefService.live ++ PlayerRefService.live ++
      TracingRefService.live ++ TimelineRefService.live ++ MazesChangesRefService.live

  /**
   * Для работы с БД через Doobie
   */
  val db: RLayer[SystemEnv with ConfigEnv, ServiceEnv] =
    ZLayer.identity[SystemEnv with ConfigEnv] >>>
      DoobieContext.live >>>
        (MazeDbService.live ++ PlayerDbService.live ++ TracingDbService.live ++
          TimelineDbService.live ++ MazesChangesDbService.live ++ MazeDescriptionDbService.live)

  /**
   * Временный фрагмент контекста со смешанными источниками
   */
  val mixed: RLayer[SystemEnv with ConfigEnv, ServiceEnv] =
    ZLayer.identity[SystemEnv with ConfigEnv] >>>
      (MazeDescriptionRefService.live ++
        (DoobieContext.live >>>
          MazeDbService.live ++ PlayerDbService.live ++ MazesChangesDbService.live ++
            TracingDbService.live ++ TimelineDbService.live))
}

/**
 * Собрание в одном месте ошибок слоя данных.
 * Здесь будут все возможные.
 * @param message сообщение об ошибке
 */
sealed abstract class DataIoError(message: String) extends EscapeError(message)

object DataIoError {
  case class EntityAlreadyExists(id: String) extends DataIoError(s"Entity with id=$id already exists")
}
