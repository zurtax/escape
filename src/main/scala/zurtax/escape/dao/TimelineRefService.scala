package zurtax.escape.dao


import java.time.OffsetDateTime

import zio.{Ref, UIO, URLayer, ZLayer}
import zurtax.escape.{ConfigEnv, Escape, SystemEnv, Timeline, TimelineEnv}
import zurtax.escape.game.{ActionInfo, Entity, PlayerId, TimeUnit, TimelineData, TimelineId}

/**
 * Класс для работы с журналами игроков
 * @param ref - стартовый набор
 */
class TimelineRefService(ref: Ref[TimelineRefService.State]) extends Timelines.Service {

  override def createOrUpdate(timeline: Timeline): Escape[TimelineId] = for {
    entity <- ref.modify(state => (timeline, state + (timeline.id.playerId.login -> timeline)))
  } yield entity.id

  /**
   * Ищем карту и возвращаем ее если нет. Если ее нет - это серьезная ошибка
   * целостности данных, но принимать решение надо уровнем выше.
   * @param timelineId игра
   * @return параметры игры, ессли такая есть
   */
  override def getOne(timelineId: TimelineId): Escape[Option[Timeline]] =
    ref.get.map(_.get(timelineId.playerId.login))

}

object TimelineRefService {
  type State = Map[String, Timeline]

  /**
   * Стартовый набор из двух игроков
   */
  val initialState:State = Map(
    "test" -> Entity(
      id = TimelineId(PlayerId("test")),
      data = TimelineData(Vector(
        TimeUnit(0, 10, 10, "game started", "player is found himself in a maze"))),
      created = ActionInfo(OffsetDateTime.MIN, None)
    ),
    "keks" -> Entity(
      id = TimelineId(PlayerId("keks")),
      data = TimelineData(Vector(
        TimeUnit(0, 10, 10, "game started", "player is found himself in a maze"))),
      created = ActionInfo(OffsetDateTime.MIN, None)
    )
  )

  /**
   * Ф-ция для добавления в контекст. Не производит исключений,
   * для запуска не требует другого контекста, производит TimelineEnv.
   */

  val live: URLayer[SystemEnv with ConfigEnv, TimelineEnv] = ZLayer.fromEffect(
    for { state <- UIO.succeed(initialState); ref <- Ref.make(state) }
      yield new TimelineRefService(ref)
  )

}
