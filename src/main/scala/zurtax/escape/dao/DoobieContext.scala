package zurtax.escape.dao

import cats.effect.Blocker
import doobie.h2.H2Transactor
import doobie.util.transactor.Transactor
import zio._
import zio.blocking.{Blocking, blocking}
import zio.interop.catz._
import zurtax.escape.ConfigEnv
import doobie.util.update.Update0
import doobie.implicits._
import cats.implicits._

import scala.io.Source
import java.nio.charset.CodingErrorAction
import scala.io.Codec

object DoobieContext {
  case class Service(transactor: Transactor[Task])

  implicit val codec:Codec = Codec("UTF-8")
  codec.onMalformedInput(CodingErrorAction.REPLACE)
  codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

  val live: RLayer[ConfigEnv with Blocking, DB] =
    ZLayer.fromManaged(
      for {
        liveEC  <- ZIO.descriptor.map(_.executor.asEC).toManaged_
        blockEC <- blocking(ZIO.descriptor.map(_.executor.asEC)).toManaged_
        conf    <- RIO.access[ConfigEnv](_.get.db).toManaged_
        trans <- H2Transactor
          .newH2Transactor[Task](
            conf.url,
            conf.user,
            conf.password,
            liveEC,
            Blocker.liftExecutionContext(blockEC)
          ).toManagedZIO
        // читаем начальный скрипт
        src = Source.fromFile("db_creation_script.sql")
        lines = src.getLines
        // выполняем начальный скрипт
        queries = lines.toList.traverse(q => Update0(q, None).run).transact(trans)
        _ <- queries.toManaged_
      } yield Service(trans)
    )
}
