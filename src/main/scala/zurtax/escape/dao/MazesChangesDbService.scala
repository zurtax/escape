package zurtax.escape
package dao

import doobie.free.connection.ConnectionIO
import doobie.implicits._
import doobie.util.transactor.Transactor
import doobie.util.update.Update
import scala.collection.immutable.ArraySeq
import zio._
import zurtax.escape.dao.MazesChangesDbService.SQL
import zurtax.escape.game.{Entities, MazeChange, MazeChangesData, MazeChangesId, PlayerId, ShotsAmount, MazeCell}


/**
 * Реализация работы с изменениями лабиринтов игроков через Doobie
 * @param xa - та часть, через которую отправляются запосы к базе
 */
class MazesChangesDbService(xa: Transactor[Task]) extends MazesChanges.Service {

  override def createOrUpdate(mazeChanges: MazeChanges): Escape[MazeChanges] = for {
    _ <- SQL.remove(mazeChanges.id).transact(xa)
    _ <- SQL.putOne(mazeChanges).transact(xa)
  } yield mazeChanges

  override def getOne(mazeChangesId: MazeChangesId): Escape[Option[MazeChanges]] =
    SQL.getOne(mazeChangesId).transact(xa)

  def getAll: Escape[Seq[MazeChanges]] = SQL.getAll.transact(xa)
}

object MazesChangesDbService {

  /**
   * Конструируем часть контекста.
   * На входе нужен контекст DB, для получения Transactor.
   * На выходе получаем MazesChangesEnv.
   */
  val live: URLayer[DB, MazesChangesEnv] =
    ZLayer.fromEffect(ZIO.access[DB](db => new MazesChangesDbService(db.get.transactor)))

  /**
   * Здесь наши запросы
   */
  object SQL {

    val getAll: ConnectionIO[Seq[MazeChanges]] = for {
      changes <- sql"SELECT player_login AS login, coords, cell FROM change_cells".query[(PlayerId, MazeChange)].to[ArraySeq]
      bolts <- sql"SELECT player_login AS login, coords, amount FROM change_shots".query[(PlayerId, ShotsAmount)].to[ArraySeq]
      changesMap = changes.groupMap(d => MazeChangesId(d._1))(d => d._2)
      boltsMap = bolts.groupMap(d => MazeChangesId(d._1))(d => d._2)
      data = (changesMap.keys ++ boltsMap.keys).toSet.toList
        .map( (k: MazeChangesId) => (k, changesMap.getOrElse(k, Vector()), boltsMap.getOrElse(k, Vector())))
        .map{ case (k,c,s) => Entities.create(k, MazeChangesData(c, s)) }
    } yield data

    val getOne: MazeChangesId => ConnectionIO[Option[MazeChanges]] = id => for {
      changes <- sql"SELECT player_login AS login, coords, cell FROM change_cells WHERE player_login=$id".query[(PlayerId, MazeChange)].to[ArraySeq]
      bolts <- sql"SELECT player_login AS login, coords, amount FROM change_shots WHERE player_login=$id".query[(PlayerId, ShotsAmount)].to[ArraySeq]
      changesMap = changes.groupMap(d => MazeChangesId(d._1))(d => d._2)
      boltsMap = bolts.groupMap(d => MazeChangesId(d._1))(d => d._2)
      data = (changesMap.keys ++ boltsMap.keys).toSet.toList
        .map( (k: MazeChangesId) => (k, changesMap.getOrElse(k, Vector()), boltsMap.getOrElse(k, Vector())))
        .map{ case (k,c,s) => Entities.create(k, MazeChangesData(c, s)) }
      res =
        if(data.size == 1) Some(data.head)  // поскольку нет отдельной таблицы для пустого MazeChanges, всегда возвращаем результат
        else Some(Entities.create(id, MazeChangesData()))
    } yield res

    def putOne(mazeChanges: MazeChanges): ConnectionIO[MazeChanges] = {
      import cats.implicits._
      val sqlChanges = "INSERT INTO change_cells(id, player_login, coords, cell) VALUES(DEFAULT,?,?,?)"
      val sqlBolts = "INSERT INTO change_shots(id, player_login, coords, amount) VALUES(DEFAULT,?,?,?)"
      val login = mazeChanges.id.playerId
      for {
        _ <- remove(mazeChanges.id)
        _ <- Update[(PlayerId, MazeChange)](sqlChanges).updateMany(mazeChanges.data.changes.map(d => (login, d)).toList)
        _ <- Update[(PlayerId, ShotsAmount)](sqlBolts).updateMany(mazeChanges.data.bolts.map(d => (login, d)).toList)
      } yield mazeChanges
    }

    def remove(id: MazeChangesId): ConnectionIO[MazeChangesId] = for {
      _ <- sql"DELETE FROM change_cells WHERE player_login=${id.playerId.login}".update.run
      _ <- sql"DELETE FROM change_shots WHERE player_login=${id.playerId.login}".update.run
    } yield id
  }

}