package zurtax.escape.dao

import zio.RIO
import zurtax.escape.game.{Entities, PlayerData, PlayerId}
import zurtax.escape.{Escape, Player, PlayerEnv}

/**
 * Общий тип для работы с данными игроков/игр.
 *
 */
object Players {

  trait Service {
    def createOrUpdate(player: Player): Escape[PlayerId]
    def getOne(playerId: PlayerId): Escape[Option[Player]]
  }

  trait Accessor[F[_]] {
    def createOrUpdate(playerId: PlayerId, playerData: PlayerData): F[PlayerId]
    def getOne(playerId: PlayerId): F[Option[PlayerData]]
  }

  implicit val playerAccessor: Players.Accessor[Escape] = new Players.Accessor[Escape] {
    private val toRIO = RIO.access[PlayerEnv](_.get)

    override def createOrUpdate(playerId: PlayerId, playerData: PlayerData): Escape[PlayerId] =
      toRIO .flatMap( rio => Entities.makeEntity(playerId, playerData).flatMap(rio.createOrUpdate))

    override def getOne(changesId: PlayerId): Escape[Option[PlayerData]] =
      toRIO .flatMap(_.getOne(changesId)) .map(_.map(_.data))
  }

}
