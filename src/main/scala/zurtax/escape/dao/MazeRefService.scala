package zurtax.escape.dao

import java.time.OffsetDateTime

import zio.{Ref, UIO, URLayer, ZLayer}
import zurtax.escape.{ConfigEnv, Escape, Maze, MazeEnv, SystemEnv}
import zurtax.escape.game.{ActionInfo, Entities, Entity, MazeData, MazeId, MazeParseOps}

/**
 * Класс для работы с набором лабиринтов из Ref
 * @param ref - стартовый набор лабиринтов
 */
class MazeRefService(ref: Ref[MazeRefService.State]) extends Mazes.Service {

  override def getOne(mazeId: MazeId): Escape[Option[Maze]] =
    ref.get.map(_.get(mazeId.mazeNo))

  override def getAll: Escape[Seq[Maze]] =
    ref.get.map(_.values.toSeq)

  override def addOne(mazeData: MazeData): Escape[MazeId] = for {
    blank   <- Entities.makeEntity(MazeId(0), mazeData)
    mazeId  <- ref.modify(m => {
        val mazeId = MazeId(m.keys.max+1)
        (mazeId, m + (mazeId.mazeNo->blank.copy(id=mazeId)))
    })
  } yield mazeId

  override def updOne(mazeId: MazeId, mazeData: MazeData): Escape[MazeId] = for {
    blank   <- Entities.makeEntity(mazeId, mazeData)
    update  <- ref.modify(m => (mazeId, m + (mazeId.mazeNo->blank)) )
  } yield mazeId
}

object MazeRefService {
  type State = Map[Int, Maze]
  val initialState: State  =  (1 to 1).map(x =>
    (x, Entity(
      id = MazeId(x),
      data = MazeData(x),
      created = ActionInfo(OffsetDateTime.MIN, None)))
  ).toMap ++ Map(
    2 -> Entity( id = MazeId(2),
      data = MazeParseOps.readLayout(
        "╔═══╗√   ║║  ■√║▒█ ║╚√══╝" ,
        5).get,
      created = ActionInfo(OffsetDateTime.MIN, None)),
    3 -> Entity( id = MazeId(3),
      data = MazeParseOps.readLayout(
        "╔═════╗√▒█   ║║▒  █ ║║█  █ ║║██ █ √║■■ ■ ║╚√════╝" ,
        7).get,
      created = ActionInfo(OffsetDateTime.MIN, None)),
    4 -> Entity( id = MazeId(4),
      data = MazeParseOps.readLayout(
        "╔√══════╗║▒▒ █   ║║▒    █ ║║   ███▒║║█    █ √║     ██║║ ██  █ ║║ █   ■ √╚═══════╝" ,
        9).get,
      created = ActionInfo(OffsetDateTime.MIN, None)),
    5 -> Entity( id = MazeId(5),
      data = MazeParseOps.readLayout(
        "╔═══════√═╗║▒▒ █  █■■║║▒        ║║     ███▒║║███    █ ║║      ███║║ ██ ▒  █ √║ █ ▒▒■■█▒║√ █  ▒  █ ║║ █  █    ║╚═════════╝" ,
        11).get,
      created = ActionInfo(OffsetDateTime.MIN, None)),
    6 -> Entity( id = MazeId(6),
      data = MazeParseOps.readLayout(
        "╔═══════√═══╗║▒▒ █  █■■  ║║▒          ║║     ███▒▒▒║║███    █ ■■║║████   █  ■║║       ██  ║║ ██ ▒    █ √║██ ▒▒  ■■█▒║√ █  ▒ █  █▒║║ ██ ███  ■■║║    █      ║╚═══════════╝" ,
        13).get,
      created = ActionInfo(OffsetDateTime.MIN, None)),
    7 -> Entity( id = MazeId(7),
      data = MazeParseOps.readLayout(
        "╔═════════════╗║ ■    ▒█    ▒║║  ██  ██ █▒ █║║█  █   █▒█ █▒√║  █      ███▒║√ █   ▒█  ▒ █▒║║█   ███      ║║   █   ██  ██║║     ▒  ▒█   ║║██ ███  █ ██ ║║      █     █║║  █  █   ██  ║║██  ██  █   █║║       █  █  ║╚════════√════╝",
        15).get,
      created = ActionInfo(OffsetDateTime.MIN, None)),
    8 -> Entity( id = MazeId(8),
      data = MazeParseOps.readLayout(
        "╔═══╗√▒ ^║║▒ ■║║ █ ║╚═══╝╔═══╗║  v║║▒█■║║  ^║╚═══╝╔═══╗║^ █║║   ║║▒ v║╚═══╝╔═══╗║v  ║║█ ■√║^  ║╚═══╝╔═══╗√   ║║█▒ ║║v  ║╚═══╝",
        5).get,
      created = ActionInfo(OffsetDateTime.MIN, None)),
    9 -> Entity( id = MazeId(9),
      data = MazeParseOps.readLayout(
        "╔═══════╗√ ▒▒ ^  ║║ ██  █▒║║   █  ▒║║ █ ██ ▒║║█ ▒ █ ▒║║ █▒██ ▒║║      ▒║╚═══════╝╔═══════╗║   █v  ║║ █   ■ ║║  █ ██■║║ █^█   √║   ■ ██║║ █ █ █ ║║ █     ║╚═══════╝╔═══════╗║^█ █^  ║║  ▒▒■█ ║║ ██    ║║ █v██ ▒║║   ■   ║║■■ ■■ ■║║   ■   ║╚═══════╝╔═══════╗║v▒▒ v  ║║█▒█ █ █║║▒▒     ║║█ ████ ║║▒ █    ║║  ^ █■■║║  █    ║╚═══════╝╔═══════╗║^▒   ^ ║║ ▒ ██■■║║ ▒     ║║ ▒■ █ ▒║║ █  ██ ║║ █v █  ║║   █   ║╚═══════╝╔═══════╗║v  ▒ v ║║█ ■▒ ■■║║       ║║█■█▒▒ ■║║^ █    ║║  █ ███║║       ║╚═══════╝╔═══════╗║▒ ^█   ║║██   ██║║   █   ║║█████■ ║║v ▒█▒  ║║ ███  ■║║     ■ ║╚═══════╝╔═══════╗║  v    ║║ █▒█■██║║       ║║██████ ║║    ^█^║║ █████ ║║ █^    ║╚√══════╝╔═══════╗║       ║║ █████ ║║     █ ║║ ███ █ ║║   █v█v║║ ██████║║ █v    ║╚═══════╝",
        9).get,
      created = ActionInfo(OffsetDateTime.MIN, None))
  )

  /**
   * А вот это ф-ция для добавления в контекст. Не производит исключений,
   * для запуска не требует другого контекста, производит MazeEnv.
   */
  val live: URLayer[SystemEnv with ConfigEnv, MazeEnv] = ZLayer.fromEffect(
    for { state <- UIO.succeed(initialState); ref <- Ref.make(state) }
      yield new MazeRefService(ref)
  )

}
