package zurtax.escape.dao

import zio.RIO
import zurtax.escape.game.{Entities, MazeChangesData, MazeChangesId}
import zurtax.escape.{Escape, MazeChanges, MazesChangesEnv}

object MazesChanges {

  trait Service {
    def createOrUpdate(mazeChanges: MazeChanges): Escape[MazeChanges]
    def getOne(mazeChangesId: MazeChangesId): Escape[Option[MazeChanges]]
  }

  trait Accessor[F[_]] {
    def createOrUpdate(changesId: MazeChangesId, changesData: MazeChangesData): F[MazeChangesId]
    def getOne(changesId: MazeChangesId): F[Option[MazeChangesData]]
  }

  implicit val mazechangesAccessor: MazesChanges.Accessor[Escape] = new MazesChanges.Accessor[Escape] {
    private val toRIO = RIO.access[MazesChangesEnv](_.get)

    override def createOrUpdate(changesId: MazeChangesId, changesData: MazeChangesData): Escape[MazeChangesId] =
      toRIO .flatMap( rio => Entities.makeEntity(changesId, changesData).flatMap(rio.createOrUpdate)) .map(_.id)

    override def getOne(changesId: MazeChangesId): Escape[Option[MazeChangesData]] =
      toRIO .flatMap(_.getOne(changesId)) .map(_.map(_.data))
  }

}
