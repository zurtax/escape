package zurtax

import cats.effect.{Concurrent, ContextShift}
import cats.{ApplicativeError, Monad}
import ru.tinkoff.tschema.finagle.zioRouting.RIOH
import tofu.logging.Logging
import tofu.logging.zlogs.ZLogs
import zio._
import zio.blocking.Blocking
import zio.clock.Clock
import zio.console.Console
import zio.interop.catz
import zurtax.escape.EscapeEnv
import zurtax.escape.config.{EscapeConfig, MessageStrings}
import zurtax.escape.dao.{MazeDescriptions, Mazes, MazesChanges, Players, Timelines, Tracings}
import zurtax.escape.game.{Entity, LogicService, LogicalError, MazeChangesData, MazeChangesId, MazeData, MazeDescriptionData, MazeDescriptionId, MazeId, PlayerData, PlayerId, TimelineData, TimelineId, TracingData, TracingId}
import zurtax.escape.http.{RequestInfo, ResourceCache}

/**
 * Все, что входит в пакет zurtax.escape получает по умолчанию доступ ко всему, что в этом пакете
 */
package object escape extends LowPriorInstances {

  /**
   * Сущность для игрока. Пока что для текущей игры игрока.
   */
  type Maze         = Entity[MazeId,   MazeData]
  type Player       = Entity[PlayerId, PlayerData]
  type Tracing      = Entity[TracingId, TracingData]
  type Timeline     = Entity[TimelineId, TimelineData]
  type MazeChanges  = Entity[MazeChangesId, MazeChangesData]
  type MazeDescription = Entity[MazeDescriptionId, MazeDescriptionData]
  type Shots        = Int
  type Health       = Int
  type Steps        = Int

  /*
   * Из "системного" контекста ZIO нам достаточно блокирующего контекста, часов и консольки
   * Нашим рабочим контекстам задаем алиасы, что бы проще было
   * Собираем итоговый контекст
   */
  type ConfigEnv      = Has[EscapeConfig]
  type StringsEnv     = Has[MessageStrings.Service]
  type LogicEnv       = Has[LogicService]
  type MazeEnv        = Has[Mazes.Service]
  type PlayerEnv      = Has[Players.Service]
  type TracingEnv     = Has[Tracings.Service]
  type TimelineEnv    = Has[Timelines.Service]
  type MazesChangesEnv= Has[MazesChanges.Service]
  type MazeDescEnv   = Has[MazeDescriptions.Service]
  type ReqInfo        = Has[RequestInfo]
  type ResourceCache  = Has[ResourceCache.Service]
  type SystemEnv      = Blocking with Clock with Console
  type ServiceEnv     = MazeEnv with PlayerEnv with TracingEnv with
    TimelineEnv with MazesChangesEnv with MazeDescEnv
  type EscapeEnv      = SystemEnv with ConfigEnv with StringsEnv with
    ServiceEnv with LogicEnv with ResourceCache with ReqInfo

  type EscapeGame[+A]     = ZIO[EscapeEnv, LogicalError, A]
  type Escape[+A]         = RIO[EscapeEnv, A]
  type EscapeRouting[+A]  = RIOH[EscapeEnv, A]
  type Init[+A]           = RManaged[SystemEnv, A]
  type EscapeLogger       = Logging[URIO[ReqInfo, *]]

  val logs                = ZLogs.build.of[ReqInfo].make[ReqInfo]

  /*
  Без этих имплиситов реализации HttpModule с не самыми элементарными методами не собираются
   */
  final implicit val escapeMonad: Concurrent[Escape]      = catz.taskConcurrentInstance
  final implicit val initMonad: Monad[Init]               = catz.monadErrorZManagedInstances
  final implicit val httpMonad: Monad[EscapeRouting]      = catz.monadErrorInstance
  final implicit val uioMonad: Monad[UIO]                 = catz.monadErrorInstance
  final implicit val taskMonad: Concurrent[Task]          = catz.taskConcurrentInstance
  final implicit val taskContextShift: ContextShift[Task] = catz.zioContextShift

  @SuppressWarnings(Array("org.wartremover.warts.Null"))
  abstract class EscapeError(message: String) extends RuntimeException(message, null, false, false)
}

/*
Без этих имплиситов реализации HttpModule с не самыми элементарными методами не собираются
 */
trait LowPriorInstances {
  final implicit def escapeErrMonad[E]: ApplicativeError[ZIO[EscapeEnv, E, *], E] = catz.monadErrorInstance
}