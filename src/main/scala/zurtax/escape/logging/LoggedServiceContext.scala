package zurtax.escape
package logging

import tofu.syntax.logging._
import zio.{RLayer, URLayer, ZIO, ZLayer}
import zurtax.escape.dao.{MazeDescriptions, Mazes, MazesChanges, Players, Timelines, Tracings}
import zurtax.escape.game.{MazeChangesId, MazeData, MazeDescriptionId, MazeId, PlayerId, TimelineId, TracingId}

object LoggedServiceContext {

  val mazeLive: URLayer[MazeEnv, MazeEnv] = ZLayer.fromEffect {
    for {
      logging     <- logs.forService[Mazes.Service]
      underlaying <- ZIO.access[MazeEnv](_.get)
    } yield new LoggedMazeService(underlaying)(logging)
  }

  val playerLive: URLayer[PlayerEnv, PlayerEnv] = ZLayer.fromEffect {
    for {
      logging     <- logs.forService[Players.Service]
      underlaying <- ZIO.access[PlayerEnv](_.get)
    } yield new LoggedPlayerService(underlaying)(logging)
  }

  val tracingLive: URLayer[TracingEnv, TracingEnv] = ZLayer.fromEffect {
    for {
      logging     <- logs.forService[Tracings.Service]
      underlying  <- ZIO.access[TracingEnv](_.get)
    } yield new LoggedTracingService(underlying)(logging)
  }

  val mazesChangesLive: URLayer[MazesChangesEnv, MazesChangesEnv] = ZLayer.fromEffect {
    for {
      logging     <- logs.forService[MazesChanges.Service]
      underlying  <- ZIO.access[MazesChangesEnv](_.get)
    } yield new LoggedMazesChangesService(underlying)(logging)
  }

  val timelineLive: URLayer[TimelineEnv, TimelineEnv] = ZLayer.fromEffect {
    for {
      logging     <- logs.forService[Timelines.Service]
      underlying <- ZIO.access[TimelineEnv](_.get)
    } yield new LoggedTimelineService(underlying)(logging)
  }

  val mazeDescriptionsLive: URLayer[MazeDescEnv, MazeDescEnv] = ZLayer.fromEffect {
    for {
      logging     <- logs.forService[MazeDescriptions.Service]
      underlying  <- ZIO.access[MazeDescEnv](_.get)
    } yield new LoggedMazeDescriptionsService(underlying)(logging)
  }

  val live: RLayer[ServiceEnv, ServiceEnv] =
    mazeLive ++ playerLive ++ tracingLive ++ timelineLive ++ mazesChangesLive ++ mazeDescriptionsLive

}


class LoggedMazeService(svc: Mazes.Service)(implicit l: EscapeLogger) extends Mazes.Service {
  override def getOne(mazeId: MazeId): Escape[Option[Maze]] = for {
    _ <- debug"accessing maze: ${mazeId.mazeNo}"
    maze <- svc.getOne(mazeId)
    _ <- debug"found maze #${mazeId.mazeNo}"
  } yield maze

  override def addOne(mazeData: MazeData): Escape[MazeId] = for {
    _ <- debug"building maze, radius: ${mazeData.radius}"
    mazeId <- svc.addOne(mazeData)
    _ <- debug"founded maze, id = ${mazeId.mazeNo}"
  } yield mazeId

  override def updOne(mazeId: MazeId, mazeData: MazeData): Escape[MazeId] = for {
    _ <- debug"rebuilding maze, radius: ${mazeData.radius}"
    mazeId <- svc.updOne(mazeId, mazeData)
    _ <- debug"reconstructed maze, id = ${mazeId.mazeNo}"
  } yield mazeId

  def getAll: Escape[Seq[Maze]] = for {
    _ <- debug"getting all mazes"
    mazeSeq <- svc.getAll
    _ <- debug"mazes retrieved"
  } yield mazeSeq
}

class LoggedPlayerService(svc: Players.Service)(implicit l: EscapeLogger) extends Players.Service {
  override def createOrUpdate(player: Player): Escape[PlayerId] = for {
    _ <- debug"updating player: ${player.id.login}"
    playerId <- svc.createOrUpdate(player)
    _ <- debug"updated player: ${playerId.login}"
  } yield playerId

  override def getOne(playerId: PlayerId): Escape[Option[Player]] = for {
    _ <- debug"accessing player: ${playerId.login}"
    player <- svc.getOne(playerId)
    _ <- debug"found player with id ${playerId.login}"
  } yield player
}

class LoggedTracingService(svc: Tracings.Service)(implicit l: EscapeLogger) extends Tracings.Service {
  override def createOrUpdate(tracing: Tracing): Escape[TracingId] = for {
    _ <- debug"updating tracing: ${tracing.id.playerId.login}"
    tracingId <- svc.createOrUpdate(tracing)
    _ <- debug"updated tracing: ${tracingId.playerId.login}"
  } yield tracingId

  override def getOne(tracingId: TracingId): Escape[Option[Tracing]] = for {
    _ <- debug"accessing tracing: ${tracingId.playerId.login}"
    tracing <- svc.getOne(tracingId)
    _ <- debug"found tracing for id = ${tracingId.playerId.login}"
  } yield tracing
}

class LoggedTimelineService(svc: Timelines.Service)(implicit l: EscapeLogger) extends Timelines.Service {
  override def createOrUpdate(timeline: Timeline): Escape[TimelineId] = for {
    _ <- debug"updating timeline for player: ${timeline.id.playerId.login}"
    timelineId <- svc.createOrUpdate(timeline)
    _ <- debug"updated timeline for player: ${timelineId.playerId.login}"
  } yield timelineId

  override def getOne(timelineId: TimelineId): Escape[Option[Timeline]] = for {
    _ <- debug"accessing timeline for player: ${timelineId.playerId.login}"
    timeline <- svc.getOne(timelineId)
    _ <- debug"found timeline for player: ${timelineId.playerId.login}"
  } yield timeline
}

class LoggedMazesChangesService(svc: MazesChanges.Service)(implicit l: EscapeLogger) extends MazesChanges.Service {
  override def createOrUpdate(changes: MazeChanges): Escape[MazeChanges] = for {
    _ <- debug"updating maze changes: ${changes.id.playerId.login}"
    mazeChanges <- svc.createOrUpdate(changes)
    _ <- debug"updated maze changes: ${mazeChanges.id.playerId.login}"
  } yield mazeChanges

  override def getOne(mazeChangesId: MazeChangesId): Escape[Option[MazeChanges]] = for {
    _ <- debug"accessing maze changes: ${mazeChangesId.playerId.login}"
    mazeChanges <- svc.getOne(mazeChangesId)
    _ <- debug"found maze changes for id = ${mazeChangesId.playerId.login}"
  } yield mazeChanges
}

class LoggedMazeDescriptionsService(svc: MazeDescriptions.Service)(implicit l: EscapeLogger) extends MazeDescriptions.Service {
  override def createOrUpdate(mazeDescription: MazeDescription): Escape[MazeDescriptionId] = for {
    _ <- debug"updating maze descriptino for No: ${mazeDescription.id.mazeId.mazeNo}"
    descriptionId <- svc.createOrUpdate(mazeDescription)
    _ <- debug"updated maze descriptino for No: ${mazeDescription.id.mazeId.mazeNo}"
  } yield descriptionId

  override def getOne(mazeDescriptionId: MazeDescriptionId): Escape[Option[MazeDescription]] = for {
    _ <- debug"searching maze description for: ${mazeDescriptionId.mazeId.mazeNo}"
    description <- svc.getOne(mazeDescriptionId)
    _ <- debug"found maze description for: ${mazeDescriptionId.mazeId.mazeNo}"
  } yield description

  override def getAll(): Escape[Seq[MazeDescription]] = for {
    _ <- debug"extract maze descriptions"
    descriptions <- svc.getAll
    _ <- debug"maze description list found"
  } yield descriptions
}