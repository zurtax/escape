package zurtax.escape.logging

import cats.syntax.show._
import tofu.syntax.logging._
import zio.{URLayer, ZIO, ZLayer}
import zurtax.escape.{Escape, EscapeLogger, LogicEnv, Player, logs}
import zurtax.escape.game.{AxisDirection, DifficultySettings, LogicService, MazeCell, MazeId, PlayerData, PlayerId}


object LoggedGameMechanics {
  val live: URLayer[LogicEnv, LogicEnv] = ZLayer.fromEffect {
    for {
      logging     <- logs.forService[LogicService]
      underlaying <- ZIO.access[LogicEnv](_.get)
    } yield new LoggedGameMechanics(underlaying)(logging)
  }
}

class LoggedGameMechanics(svc: LogicService)(implicit l: EscapeLogger) extends LogicService {

  override def createNewGame(playerId: PlayerId, mazeId: MazeId,
                             difficulty: DifficultySettings): Escape[Player] = for {
    _ <- info"Game: Starting new game for the player ${playerId.login} in the maze: ${mazeId.mazeNo}"
    game <- svc.createNewGame(playerId, mazeId, difficulty)
    _ <- info"Game: Player ${playerId.login} is placed in a maze ${mazeId.mazeNo}"
  } yield game

  override def getPlayerStatus(playerId: PlayerId): Escape[PlayerData] = for {
    _ <- info"Status: Loading for the player ${playerId.login}"
    game <- svc.getPlayerStatus(playerId)
    _ <- info"Status: Player ${playerId.login} game is loaded successfully"
  } yield game

  override def getPlayerAutomap(playerId: PlayerId): Escape[String] = for {
    _ <- info"Automap: Draw map for the player ${playerId.login}"
    automap <- svc.getPlayerAutomap(playerId)
    _ <- info"Automap: Auto-map for the player ${playerId.login} is drawled"
  } yield automap

  override def checkMazeCell(playerId: PlayerId, direction: AxisDirection): Escape[MazeCell] = for {
    _ <- info"Game: Player ${playerId.login} throws a bolt or nut to the  ${direction.show}"
    cell <- svc.checkMazeCell(playerId, direction)
    _ <- info"Game: Player ${playerId.login} revealed a cell in the ${direction.show}"
  } yield cell

  override def makeOneStep(playerId: PlayerId, direction: AxisDirection): Escape[PlayerData] = for {
    _ <- info"Game: Player ${playerId.login} is trying to go ${direction.show}"
    game <- svc.makeOneStep(playerId, direction)
    _ <- info"Game: Player ${playerId.login} took a step ${direction.show}"
  } yield game

  def mazeAddString(radius: Int, layout: String): Escape[MazeId] = for {
    _ <- info"Maze: Try to construct new maze from: $layout"
    maze <- svc.mazeAddString(radius, layout)
    _ <- info"Maze: New maze has been constructed from: $layout"
  } yield maze

  def mazeUpdString(maze: MazeId, radius: Int, layout: String): Escape[MazeId] = for {
    _ <- info"Maze: Try to reconstruct existing maze #${maze.mazeNo} from layout[$radius]: $layout"
    maze <- svc.mazeUpdString(maze, radius, layout)
    _ <- info"Maze: Maze #${maze.mazeNo} has been reconstructed with layout[$radius]: $layout"
  } yield maze

  def all(): Escape[Seq[(Int, String)]] = for {
    _ <- info"Maze: Getting all maze numbers"
    seq <- svc.all()
    _ <- info"Maze: Maze numbers retrieved"
  } yield seq

  override def getPlayerTimeline(playerId: PlayerId): Escape[Seq[String]] = for {
    _ <- info"Timeline: Looking for timeline for player ${playerId.login}"
    maze <- svc.getPlayerTimeline(playerId)
    _ <- info"Timeline: Find timeline for player ${playerId.login}"
  } yield maze

}