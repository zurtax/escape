function docLoad() {
    const urlParams = new URLSearchParams(window.location.search);
    if(!urlParams.has('player'))
        alert('No player!')
    else {
    const plid = urlParams.get('player')
    document.getElementById('playerid').innerHTML = plid;
       fetch('api/player/'+plid+'/automap')
         .then(response => response.text())
         .then(automap => {
           document.getElementById('automap').innerHTML = '<pre>' + automap + '</pre>'
         });
       fetch('api/player/'+plid+'/timeline?sort=desc')
         .then(response => response.json())
         .then(timeline => {
           var result = ''
           for( line of timeline ){
             result+= '<li>' + line + '</li>'
           }
           document.getElementById('timeline').innerHTML = result
         });
       fetch('api/player/'+plid)
         .then(response => response.json())
         .then(status => {
           document.getElementById('hp').innerHTML = status.hp
           document.getElementById('nuts').innerHTML = status.shots
         });
     }
   }

window.onload = function(){docLoad()}
