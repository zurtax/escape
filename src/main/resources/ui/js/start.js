function createGame() {
    var elements = document.getElementById('newGame').elements;
    var url = 'api/player/'
    var player;
    for(var i = 0 ; i < elements.length; i++){
        var item = elements.item(i);
        if( item.name == 'player'){
            player = item.value
            url+= item.value + "?";
        }
        if( item.name == 'mazeNo') url+= item.name + "=" + item.value;
    }
    fetch(url, {
        method: 'POST',
        redirect: 'manual'
    })
    .then(response => {if(response.status == 200) window.location.replace('game.html?player=' + player);});
}

function getMazeNumbers(){
    fetch('api/maze/all')
        .then(response => response.json())
        .then(numbers => {
          var result = ''
          for( n of numbers ){
            let str = n.mazeDesc==''?n.mazeNo:(n.mazeNo + ': ' + n.mazeDesc)
            result+= '<option value=\"' + n.mazeNo + '\">' + str + '</option>'
          }
          document.getElementById('mazeNo').innerHTML = result
        });
}

window.onload = function(){getMazeNumbers()}